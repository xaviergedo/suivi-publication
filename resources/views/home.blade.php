@extends('layouts.admin')
@section('content')
<div class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="card">
                <div class="card-header">
                    Tableau de bord
                </div>
                <br>
                @if(Auth::user()->roles()->first()->id == 4)
                <div class="col-lg-12">
                    <a class="btn btn-success" href="{{ route('admin.courriers.create') }}">
                        {{ trans('global.add') }} publication
                    </a>
                    <a class="btn btn-success" href="{{ route('admin.courriers.index') }}">
                        Mes publications
                    </a>
                </div>
                @endif

                <div class="card-body">
                    @if(session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    {{-- @if(Auth::user()->roles()->first()->id != 3)
                    <div class="row">
                        <div class="{{ $chart1->options['column_class'] }}">
                            <h3>{!! $chart1->options['chart_title'] !!}</h3>
                            {!! $chart1->renderHtml() !!}
                        </div>
                        <div class="{{ $chart2->options['column_class'] }}">
                            <h3>{!! $chart2->options['chart_title'] !!}</h3>
                            {!! $chart2->renderHtml() !!}
                        </div>
                        <div class="{{ $chart3->options['column_class'] }}">
                            <h3>{!! $chart3->options['chart_title'] !!}</h3>
                            {!! $chart3->renderHtml() !!}
                        </div>
                    </div>
                    @endif --}}

                    <br>
                    <div class="row">
                        <div class="card text-white bg-primary mb-3 ml-1" style="max-width: 18rem;">
                            <div class="card-header">Nombre de publication saisi</div>
                            <div class="card-body">
                              <h5 class="card-title text-center my-auto">10</h5>
                            </div>
                        </div><br>
                        <div class="card text-white bg-primary mb-3 ml-1" style="max-width: 18rem;">
                            <div class="card-header">Nombre de publication affecté</div>
                            <div class="card-body">
                              <h5 class="card-title text-center my-auto">10</h5>
                            </div>
                        </div>
                        <div class="card text-white bg-primary mb-3 ml-1" style="max-width: 18rem;">
                            <div class="card-header">Nombre de publication non affecté</div>
                            <div class="card-body">
                              <h5 class="card-title text-center my-auto">10</h5>
                            </div>
                        </div>
                        <div class="card text-white bg-primary mb-3 ml-1" style="max-width: 18rem;">
                            <div class="card-header">Nombre de publication deja traité</div>
                            <div class="card-body">
                              <h5 class="card-title text-center my-auto">10</h5>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('scripts')
@parent
{{-- <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.5.0/Chart.min.js"></script>{!! $chart1->renderJs() !!}{!! $chart2->renderJs() !!}{!! $chart3->renderJs() !!} --}}
{{-- <link href="/style/js/chart.min.js" rel="stylesheet" /> --}}
<script src="{{ asset('style/js/chart.min.js') }}"></script>{!! $chart1->renderJs() !!}{!! $chart2->renderJs() !!}{!! $chart3->renderJs() !!}
@endsection
