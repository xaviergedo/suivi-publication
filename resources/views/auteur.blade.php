<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <title>Inscription</title>

    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>

    <!-- Styles -->
    <style>
        html,
        body {
            background-color: #fff;
            background-image: url("/background.jpg");
            background-size: cover;
            background-repeat: no-repeat;
            background-position: center;
            color: #636b6f;
            font-family: montserrat, calibri, arial, tahoma, verdana, sans-serif;
            font-weight: 200;
            height: 100vh;
            margin: 0;
        }

        .full-height {
            height: 100vh;
        }

        .flex-center {
            align-items: center;
            display: flex;
            justify-content: center;
        }

        .position-ref {
            position: relative;
        }

        .top-right {
            position: absolute;
            right: 10px;
            top: 18px;
        }

        .content {
            text-align: center;
        }

        .title {
            font-size: 84px;
        }

        .links>a {
            color: #636b6f;
            padding: 0 25px;
            font-size: 13px;
            font-weight: 600;
            letter-spacing: .1rem;
            text-decoration: none;
            text-transform: uppercase;
        }

        .m-b-md {
            margin-bottom: 30px;
        }

        fieldset {
            border: 1px solid gray;
            -moz-border-radius: 8px;
            -webkit-border-radius: 8px;
            border-radius: 8px;
        }

        label {
            font-size: 12px !important;
            font-weight: 700;
            margin-top: 10px;
            margin-bottom: 3px;
            padding-bottom: 0px;
        }

        no-border {
            border: none !important;
        }

        .lowercase {
            text-transform: lowercase;
        }

        .uppercase {
            text-transform: uppercase;
        }

        .capitalize {
            text-transform: capitalize;
        }
    </style>

</head>

<body>

    <nav class="navbar navbar-expand-lg navbar-light bg-none">
        <a class="navbar-brand" href="/"><img src="p.jpeg" alt=""></a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText"
            aria-controls="navbarText" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarText">
            <ul class="navbar-nav mr-auto">
            </ul>
            <span class="navbar-text">
                <a href="/login" class="text">Retour</a>
            </span>
        </div>
    </nav>

    <div class="mb-5">
        <div class="card no-border">
            <form class="col-md-10 mx-auto" action="{{route('store-auteur')}}" method="POST"
                enctype="multipart/form-data" onsubmit="myButton.disabled = true; return true;">
                <h2 class="text-center mt-3">Formulaire d'inscription des auteurs</h2>
                {{-- @include('partials._messages') --}}
                <hr>
                @csrf
                <fieldset class="px-4 pb-4 mb-3">
                    <legend class="px-3" style="color:#fff;background-color:steelblue">JE M'IDENTIFIE</legend>
                    <div class="form-row">

                        <div class="col-md-5">
                            <label for="nom" class="col-form-label">NOM</label>
                            <input value="{{old('nom')}}" id="nom" type="text"
                                class="form-control uppercase @error('nom') is-invalid @enderror" name="nom" required
                                placeholder="Nom" autocomplete="off">
                            @error('nom')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-7">
                            <label for="prenom" class="col-form-label">PRENOM</label>
                            <input value="{{old('prenom')}}" id="prenom" type="text"
                                class="form-control capitalize @error('prenom') is-invalid @enderror" name="prenom"
                                required placeholder="Prenom" autocomplete="off">
                            @error('prenom')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-4">
                            <label for="genre" class="col-form-label">GENRE</label>
                            <select id="genre" name="genre"
                                class="form-control select2 @error('genre') is-invalid @enderror" required="required">
                                <option value="">Choisissez le genre</option>
                                <option value="M">Masculin</option>
                                <option value="f">Féminin</option>
                            </select>
                            @error('genre')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-8">
                            <label for="password" class="col-form-label">MOT DE PASSE </label>
                            <input id="password" name="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" required placeholder="{{ trans('global.login_password') }}">
                            @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <fieldset class="px-4 pb-4 mb-3">
                    <legend class="px-3" style="color:#fff;background-color:steelblue">COMMENT ME JOINDRE ?</legend>
                    <div class="form-row">

                        <div class="col-md-5">
                            <label for="telephone" class="col-form-label">MON NUMERO DE TELEPHONE </label>
                            <input value="{{old('telephone')}}" id="telephone" type="text"
                                class="form-control @error('telephone') is-invalid @enderror" name="telephone" required
                                placeholder="Numéro de téléphone" autocomplete="off">
                            @error('telephone')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="col-md-7">
                            <label for="email" class="col-form-label">MAIL</label>
                            <input value="{{old('email')}}" id="email" type="email"
                                class="form-control @error('email') is-invalid @enderror" name="email" required
                                placeholder="Adresse mail" autocomplete="off">
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>

                    </div>
                </fieldset>

                <div class="col-md-4 mx-auto">
                    <div class="input-group">
                        <button type="submit" class="form-control btn-success btn-md btn-validation text-uppercase"
                            name="myButton"><b>Je m'inscris</b></button>
                    </div>
                </div>

            </form>
        </div>
    </div>



    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/css/select2.min.css" rel="stylesheet"/>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.10/js/select2.min.js"></script>



</body>

</html>

<script>

$(document).ready(function(){
$("#myselect").select2();
$("#myselect1").select2();
$("#myselect2").select2();
});
  </script>
