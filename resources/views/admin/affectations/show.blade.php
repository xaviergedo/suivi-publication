@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.affectation.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.affectations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.affectation.fields.id') }}
                        </th>
                        <td>
                            {{ $affectation->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Titre
                        </th>
                        <td>
                            @foreach($affectation->courriers as $key => $courrier)
                                <span class="label label-info">{{ $courrier->objet }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            Rélecteurs
                        </th>
                        <td>
                            @foreach($affectation->users as $key => $direction)
                                <span class="label label-info">{{ $direction->name }}</span>
                            @endforeach
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.affectation.fields.date_affectation') }}
                        </th>
                        <td>
                            {{ $affectation->date_affectation }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.affectation.fields.commentaire') }}
                        </th>
                        <td>
                            {!! $affectation->commentaire !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.affectation.fields.user') }}
                        </th>
                        <td>
                            {{ $affectation->user->name ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.affectations.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
