@extends('layouts.admin')
@section('content')
{{-- @can('affectation_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.affectations.create') }}">
                {{ trans('global.add') }} {{ trans('cruds.affectation.title_singular') }}
            </a>
        </div>
    </div>
@endcan --}}
<div class="card">
    @if(Auth::user()->roles()->first()->id == 1)
    <div class="card-header">
        Publications affectées
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Affectation">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.affectation.fields.id') }}
                        </th>
                        <th>
                            Titre
                        </th>
                        <th>
                            Rélecteur(s)
                        </th>
                        <th>
                            {{ trans('cruds.affectation.fields.date_affectation') }}
                        </th>
                        <th>
                            {{ trans('cruds.affectation.fields.user') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            {{-- <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($courriers as $key => $item)
                                    <option value="{{ $item->numero }}">{{ $item->numero }}</option>
                                @endforeach
                            </select> --}}
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($users as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($users as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($affectations as $key => $affectation)
                        <tr data-entry-id="{{ $affectation->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $affectation->id ?? '' }}
                            </td>
                            <td>
                                @foreach($affectation->courriers as $key => $item)
                                    <span class="badge badge-info">{{ $item->objet }}</span>
                                @endforeach
                            </td>
                            <td>
                                @foreach($affectation->users as $key => $item)
                                    <span class="badge badge-info">{{ $item->name }}</span>
                                @endforeach
                            </td>
                            <td>
                                {{ $affectation->date_affectation ?? '' }}
                            </td>
                            <td>
                                {{ $affectation->user->name ?? '' }}
                            </td>
                            <td>
                                @if(Auth::user()->roles()->first()->id == 1)
                                    <a class="btn btn-xs btn-warning" href="#" data-toggle="modal" data-target="#form3">
                                        Délibérer
                                    </a>
                                @endif

                                @can('affectation_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.affectations.show', $affectation->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('affectation_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.affectations.edit', $affectation->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('affectation_delete')
                                    <form action="{{ route('admin.affectations.destroy', $affectation->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                                <div class="modal fade" id="form3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                      <div class="modal-content">
                                        <div class="modal-header border-bottom-0">
                                          <h5 class="modal-title text-success" id="exampleModalLabel">Evaluation de la publication</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                          </button>
                                        </div>
                                        <div class="card">
                                              {{-- <h5 class="card-title"> <u>Titre</u> : {{$courrier->objet }}</h5> --}}
                                        </div>

                                        <form action="{{route('admin.courriers.traiter', $affectation->id)}}" method="POST">
                                            @csrf
                                          <div class="modal-body">
                                            <div class="form-row">
                                                <input type="hidden" name="courrier_id" id="courrier_id" value="{{$affectation->id}}">

                                            <div class="col-6">
                                                    <label for="date_traitement">Date évaluation</label>
                                                    <input class="form-control datetime {{ $errors->has('date_traitement') ? 'is-invalid' : '' }}" type="text" name="date_traitement" id="date_traitement" value="{{ old('date_traitement') }}" required>
                                                    @if($errors->has('date_traitement'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('date_traitement') }}
                                                        </div>
                                                    @endif
                                                    <span class="help-block">{{ trans('cruds.traitement.fields.date_traitement_helper') }}</span>
                                            </div>

                                            <div class="col-6">
                                                    <label for="etats">Avis</label>
                                                    <select id="etat_id" name="etat_id" class="form-control select2 @error('etat_id') is-invalid @enderror">
                                                        <option value="" >Choisissez un avis</option>
                                                        @foreach ($etats as $etat)
                                                            <option value="{{$etat->id}}" >{{$etat->libelle}}</option>
                                                        @endforeach
                                                    </select>
                                                    @error('etat')
                                                    <span class="invalid-feedback" role="alert">
                                                    <strong>{{ $message }}</strong>
                                                    </span>
                                                    @enderror
                                            </div>

                                            <div class="col-12">
                                                    <label for="commentaire">{{ trans('cruds.traitement.fields.commentaire') }}</label>
                                                    <textarea required class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" name="commentaire" id="commentaire">{!! old('commentaire') !!}</textarea>
                                                    @if($errors->has('commentaire'))
                                                        <div class="invalid-feedback">
                                                            {{ $errors->first('commentaire') }}
                                                        </div>
                                                    @endif
                                                    <span class="help-block">{{ trans('cruds.traitement.fields.commentaire_helper') }}</span>
                                            </div>
                                        </div>

                                          </div>
                                          <div class="modal-footer border-top-0 d-flex justify-content-center">
                                            <button type="submit" name="traiter" class="btn btn-success">Valider</button>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>

                            </td>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
    @endif

     {{-- role direction teechnique --}}

    @if(Auth::user()->roles()->first()->id == 3)
    <style>
        h2 {
         text-align: justify !important;
        }
    </style>

<div class="card">
    <div class="card-header">
       Publications affectées
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Courrier">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.courrier.fields.id') }}
                        </th>
                        <th>
                            Titre
                        </th>
                        <th>
                            Mots clés
                        </th>
                        <th>
                            Co-acteur
                        </th>
                        <th>
                            {{ trans('cruds.courrier.fields.user') }}
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($users as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($affectation_courriers as $key => $courrier)
                        <tr data-entry-id="{{ $courrier->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $courrier->id ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->objet ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->references ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->acteur ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->user->name ?? '' }}
                            </td>
                            <td>
                                @if(Auth::user()->roles()->first()->id == 3)
                                    @if($traitement_by_relecteur == 0)
                                    <a class="btn btn-xs btn-warning" href="#" data-toggle="modal" data-target="#form3">
                                        Evaluer
                                    </a>
                                    @endif
                                @endif
                            </td>

                            <div class="modal fade" id="form3" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header border-bottom-0">
                                      <h5 class="modal-title text-success" id="exampleModalLabel">Evaluation de la publication</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="card">
                                          <h5 class="card-title"> <u>Titre</u> : {{$courrier->objet }}</h5>
                                    </div>

                                    <form action="{{route('admin.courriers.traiter', $courrier->id)}}" method="POST">
                                        @csrf
                                      <div class="modal-body">
                                        <div class="form-row">
                                            <input type="hidden" name="courrier_id" id="courrier_id" value="{{$courrier->id}}">

                                        <div class="col-6">
                                                <label for="date_traitement">Date évaluation</label>
                                                <input class="form-control datetime {{ $errors->has('date_traitement') ? 'is-invalid' : '' }}" type="text" name="date_traitement" id="date_traitement" value="{{ old('date_traitement') }}" required>
                                                @if($errors->has('date_traitement'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('date_traitement') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.traitement.fields.date_traitement_helper') }}</span>
                                        </div>

                                        <div class="col-6">
                                                <label for="etats">Avis</label>
                                                <select id="etat_id" name="etat_id" class="form-control select2 @error('etat_id') is-invalid @enderror">
                                                    <option value="" >Choisissez un avis</option>
                                                    @foreach ($etats as $etat)
                                                        <option value="{{$etat->id}}" >{{$etat->libelle}}</option>
                                                    @endforeach
                                                </select>
                                                @error('etat')
                                                <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                                </span>
                                                @enderror
                                        </div>

                                        <div class="col-12">
                                                <label for="commentaire">{{ trans('cruds.traitement.fields.commentaire') }}</label>
                                                <textarea required class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" name="commentaire" id="commentaire">{!! old('commentaire') !!}</textarea>
                                                @if($errors->has('commentaire'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('commentaire') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.traitement.fields.commentaire_helper') }}</span>
                                        </div>
                                    </div>

                                      </div>
                                      <div class="modal-footer border-top-0 d-flex justify-content-center">
                                        <button type="submit" name="traiter" class="btn btn-success">Valider</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>

    @endif

</div>

@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('affectation_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.affectations.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Affectation:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

let visibleColumnsIndexes = null;
$('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value

      let index = $(this).parent().index()
      if (visibleColumnsIndexes !== null) {
        index = visibleColumnsIndexes[index]
      }

      table
        .column(index)
        .search(value, strict)
        .draw()
  });
table.on('column-visibility.dt', function(e, settings, column, state) {
      visibleColumnsIndexes = []
      table.columns(":visible").every(function(colIdx) {
          visibleColumnsIndexes.push(colIdx);
      });
  })
})

</script>
@endsection



@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('courrier_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.courriers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Courrier:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

let visibleColumnsIndexes = null;
$('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value

      let index = $(this).parent().index()
      if (visibleColumnsIndexes !== null) {
        index = visibleColumnsIndexes[index]
      }

      table
        .column(index)
        .search(value, strict)
        .draw()
  });
table.on('column-visibility.dt', function(e, settings, column, state) {
      visibleColumnsIndexes = []
      table.columns(":visible").every(function(colIdx) {
          visibleColumnsIndexes.push(colIdx);
      });
  })
})

</script>
@endsection
