@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.affectation.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.affectations.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label class="required" for="courriers">{{ trans('cruds.affectation.fields.courrier') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('courriers') ? 'is-invalid' : '' }}" name="courriers[]" id="courriers" multiple required>
                    @foreach($courriers as $id => $courrier)
                        <option value="{{ $id }}" {{ in_array($id, old('courriers', [])) ? 'selected' : '' }}>{{ $courrier }}</option>
                    @endforeach
                </select>
                @if($errors->has('courriers'))
                    <div class="invalid-feedback">
                        {{ $errors->first('courriers') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.affectation.fields.courrier_helper') }}</span>
            </div>
            <div class="form-group">
                <label class="required" for="directions">{{ trans('cruds.affectation.fields.direction') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('directions') ? 'is-invalid' : '' }}" name="directions[]" id="directions" multiple required>
                    @foreach($directions as $id => $direction)
                        <option value="{{ $id }}" {{ in_array($id, old('directions', [])) ? 'selected' : '' }}>{{ $direction }}</option>
                    @endforeach
                </select>
                @if($errors->has('directions'))
                    <div class="invalid-feedback">
                        {{ $errors->first('directions') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.affectation.fields.direction_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_affectation">{{ trans('cruds.affectation.fields.date_affectation') }}</label>
                <input class="form-control datetime {{ $errors->has('date_affectation') ? 'is-invalid' : '' }}" type="text" name="date_affectation" id="date_affectation" value="{{ old('date_affectation') }}">
                @if($errors->has('date_affectation'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_affectation') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.affectation.fields.date_affectation_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_reception">{{ trans('cruds.affectation.fields.date_reception') }}</label>
                <input class="form-control datetime {{ $errors->has('date_reception') ? 'is-invalid' : '' }}" type="text" name="date_reception" id="date_reception" value="{{ old('date_reception') }}">
                @if($errors->has('date_reception'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_reception') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.affectation.fields.date_reception_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="commentaire">{{ trans('cruds.affectation.fields.commentaire') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" name="commentaire" id="commentaire">{!! old('commentaire') !!}</textarea>
                @if($errors->has('commentaire'))
                    <div class="invalid-feedback">
                        {{ $errors->first('commentaire') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.affectation.fields.commentaire_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.affectations.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $affectation->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

@endsection