@extends('layouts.admin')
@section('content')

<style>
    h2 {
     text-align: justify !important;
    }

</style>
@if(Auth::user()->roles()->first()->id == 4)
@can('courrier_create')
    <div style="margin-bottom: 10px;" class="row">
        <div class="col-lg-12">
            <a class="btn btn-success" href="{{ route('admin.courriers.create') }}">
                {{ trans('global.add') }} publication
            </a>
        </div>
    </div>
@endcan
@endif
<div class="card">
    <div class="card-header">
        {{ trans('cruds.courrier.title_singular') }} {{ trans('global.list') }}
    </div>

    <div class="card-body">
        <div class="table-responsive">
            <table class=" table table-bordered table-striped table-hover datatable datatable-Courrier">
                <thead>
                    <tr>
                        <th width="10">

                        </th>
                        <th>
                            {{ trans('cruds.courrier.fields.id') }}
                        </th>
                        <th>
                            Titre
                        </th>
                        <th>
                            Mots clés
                        </th>
                        <th>
                            Co-auteurs
                        </th>
                        <th>
                            {{ trans('cruds.courrier.fields.user') }}
                        </th>
                        <th>
                            Crée le
                        </th>
                        <th>
                            &nbsp;
                        </th>
                    </tr>
                    <tr>
                        <td>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                            <select class="search">
                                <option value>{{ trans('global.all') }}</option>
                                @foreach($users as $key => $item)
                                    <option value="{{ $item->name }}">{{ $item->name }}</option>
                                @endforeach
                            </select>
                        </td>
                        <td>
                            <input class="search" type="text" placeholder="{{ trans('global.search') }}">
                        </td>
                        <td>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    @foreach($courriers as $key => $courrier)
                        <tr data-entry-id="{{ $courrier->id }}">
                            <td>

                            </td>
                            <td>
                                {{ $courrier->id ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->objet ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->references ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->acteur ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->user->name ?? '' }}
                            </td>
                            <td>
                                {{ $courrier->created_at ?? '' }}
                            </td>
                            <td>

                                @if(Auth::user()->roles()->first()->id == 1)
                                @if ($courrier->affectation_id ==null)
                                @can('courrier_show')
                                <a class="btn btn-xs btn-success" href="#" data-toggle="modal" data-target="#form1">
                                    Affecter
                                </a>
                                @endcan
                                @endif
                                @endif
{{--
                                @can('courrier_show')
                                    <a class="btn btn-xs btn-warning" href="#" data-toggle="modal" data-target="#form3">
                                        Evaluer
                                    </a>
                                @endcan --}}
                                @can('courrier_show')
                                    <a class="btn btn-xs btn-primary" href="{{ route('admin.courriers.show', $courrier->id) }}">
                                        {{ trans('global.view') }}
                                    </a>
                                @endcan

                                @can('courrier_edit')
                                    <a class="btn btn-xs btn-info" href="{{ route('admin.courriers.edit', $courrier->id) }}">
                                        {{ trans('global.edit') }}
                                    </a>
                                @endcan

                                @can('courrier_delete')
                                    <form action="{{ route('admin.courriers.destroy', $courrier->id) }}" method="POST" onsubmit="return confirm('{{ trans('global.areYouSure') }}');" style="display: inline-block;">
                                        <input type="hidden" name="_method" value="DELETE">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="submit" class="btn btn-xs btn-danger" value="{{ trans('global.delete') }}">
                                    </form>
                                @endcan

                            </td>

                            <div class="modal fade" id="form1" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-dialog-centered" role="document">
                                  <div class="modal-content">
                                    <div class="modal-header border-bottom-0">
                                      <h5 class="modal-title text-success" id="exampleModalLabel">Affectation de publication</h5>
                                      <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                      </button>
                                    </div>
                                    <div class="card">
                                          <h5 class="card-title"> <u>Titre</u> : {{$courrier->objet }}</h5>
                                    </div>

                                    <form action="{{route('admin.courriers.affecter', $courrier->id)}}" method="POST">
                                        @csrf
                                      <div class="modal-body">
                                        <div class="form-row">
                                            <input type="hidden" name="courrier_id" id="courrier_id" value="{{$courrier->id}}">
                                            <div class="col-md-12">
                                                <label class="required" for="users"><h5>Choisir les rélecteurs </h5></label>
                                                <div style="padding-bottom: 4px">
                                                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                                                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                                                </div>
                                                <select class="form-control select2 {{ $errors->has('users') ? 'is-invalid' : '' }}" name="users[]" id="users" multiple required>
                                                    @foreach($users as $id => $user)
                                                        <option value="{{ $user->id }}" {{ in_array($user->id, old('users', [])) ? 'selected' : '' }}>{{ $user->name }}</option>
                                                    @endforeach
                                                </select>
                                                @if($errors->has('directions'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('directions') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.affectation.fields.direction_helper') }}</span>
                                            </div>

                                            <div class="col-12">
                                                <h5><label for="date_affectation">{{ trans('cruds.affectation.fields.date_affectation') }}</label></h5>
                                                <input class="form-control datetime {{ $errors->has('date_affectation') ? 'is-invalid' : '' }}" type="text" name="date_affectation" id="date_affectation" value="{{ old('date_affectation') }}">
                                                @if($errors->has('date_affectation'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('date_affectation') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.affectation.fields.date_affectation_helper') }}</span>
                                            </div>

                                            <div class="col-12">
                                                <h5><label for="commentaire">{{ trans('cruds.affectation.fields.commentaire') }}</label></h5>
                                                <textarea  class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" rows="3" name="commentaire" id="commentaire">{!! old('commentaire') !!}</textarea>
                                                @if($errors->has('commentaire'))
                                                    <div class="invalid-feedback">
                                                        {{ $errors->first('commentaire') }}
                                                    </div>
                                                @endif
                                                <span class="help-block">{{ trans('cruds.affectation.fields.commentaire_helper') }}</span>
                                            </div>
                                    </div>

                                      </div>
                                      <div class="modal-footer border-top-0 d-flex justify-content-center">
                                        <button type="submit" name="affecter" class="btn btn-success">Valider</button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>

                        </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
</div>



@endsection
@section('scripts')
@parent
<script>
    $(function () {
  let dtButtons = $.extend(true, [], $.fn.dataTable.defaults.buttons)
@can('courrier_delete')
  let deleteButtonTrans = '{{ trans('global.datatables.delete') }}'
  let deleteButton = {
    text: deleteButtonTrans,
    url: "{{ route('admin.courriers.massDestroy') }}",
    className: 'btn-danger',
    action: function (e, dt, node, config) {
      var ids = $.map(dt.rows({ selected: true }).nodes(), function (entry) {
          return $(entry).data('entry-id')
      });

      if (ids.length === 0) {
        alert('{{ trans('global.datatables.zero_selected') }}')

        return
      }

      if (confirm('{{ trans('global.areYouSure') }}')) {
        $.ajax({
          headers: {'x-csrf-token': _token},
          method: 'POST',
          url: config.url,
          data: { ids: ids, _method: 'DELETE' }})
          .done(function () { location.reload() })
      }
    }
  }
  dtButtons.push(deleteButton)
@endcan

  $.extend(true, $.fn.dataTable.defaults, {
    orderCellsTop: true,
    order: [[ 1, 'desc' ]],
    pageLength: 100,
  });
  let table = $('.datatable-Courrier:not(.ajaxTable)').DataTable({ buttons: dtButtons })
  $('a[data-toggle="tab"]').on('shown.bs.tab click', function(e){
      $($.fn.dataTable.tables(true)).DataTable()
          .columns.adjust();
  });

let visibleColumnsIndexes = null;
$('.datatable thead').on('input', '.search', function () {
      let strict = $(this).attr('strict') || false
      let value = strict && this.value ? "^" + this.value + "$" : this.value

      let index = $(this).parent().index()
      if (visibleColumnsIndexes !== null) {
        index = visibleColumnsIndexes[index]
      }

      table
        .column(index)
        .search(value, strict)
        .draw()
  });
table.on('column-visibility.dt', function(e, settings, column, state) {
      visibleColumnsIndexes = []
      table.columns(":visible").every(function(colIdx) {
          visibleColumnsIndexes.push(colIdx);
      });
  })
})

</script>
@endsection
