@extends('layouts.admin')
@section('content')


<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.courrier.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-primary" href="{{ route('admin.courriers.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
        <div class="row">
            <!-- left column -->
            <div class="col-md-7">
              <!-- general form elements -->
              <div class="card card-primary">
                <div class="card-header text-white bg-primary">
                  <h3 class="card-title">Détails de la publication</h3>
                </div>
                <!-- /.card-header -->
                <!-- form start -->
                <form role="form">
                  <div class="card-body">

                    <table class="table table-bordered table-striped">
                        <tbody>
                            {{-- <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.id') }}
                                </th>
                                <td>
                                    {{ $courrier->id }}
                                </td>
                            </tr> --}}
                            {{-- <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.numero') }}
                                </th>
                                <td>
                                    {{ $courrier->numero }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.numero_registre') }}
                                </th>
                                <td>
                                    {{ $courrier->numero_registre }}
                                </td>
                            </tr> --}}
                            <tr>
                                <th>
                                    Titre
                                </th>
                                <td>
                                    {{ $courrier->objet }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Mots clés
                                </th>
                                <td>
                                    {{ $courrier->references }}
                                </td>
                            </tr>
                            {{-- <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.type') }}
                                </th>
                                <td>
                                    {{ App\Models\Courrier::TYPE_RADIO[$courrier->type] ?? '' }}
                                </td>
                            </tr> --}}
                            <tr>
                                <th>
                                    Type publication
                                </th>
                                <td>
                                    {{ $courrier->type_publication->libelle }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Résumé
                                </th>
                                <td>
                                    {{ $courrier->commentaire }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                   Co-acteurs {{-- {{ trans('cruds.courrier.fields.acteur') }} --}}
                                </th>
                                <td>
                                    {{ $courrier->acteur }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.user') }}
                                </th>
                                <td>
                                    {{ $courrier->user->name ?? '' }}
                                </td>
                            </tr>
                            {{-- <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.commentaire') }}
                                </th>
                                <td>
                                    {!! $courrier->commentaire !!}
                                </td>
                            </tr> --}}
                            {{-- <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.mention') }}
                                </th>
                                <td>
                                    {{ $courrier->mention->libelle ?? '' }}
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.archiver') }}
                                </th>
                                <td>
                                    {{ App\Models\Courrier::ARCHIVER_RADIO[$courrier->archiver] ?? '' }}
                                </td>
                            </tr> --}}
                            <tr>
                                <th>
                                    {{ trans('cruds.courrier.fields.pieces') }}
                                </th>
                                <td>
                                    @foreach($courrier->pieces as $key => $media)
                                        <a href="{{ $media->getUrl() }}" target="_blank">
                                            {{ trans('global.view_file') }}
                                        </a>
                                    @endforeach
                                </td>
                            </tr>
                            <tr>
                                <th>
                                    Décision
                                </th>
                                <td>
                                    {{ $courrier->last_etat }}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                  </div>
                  <!-- /.card-body -->
                </form>
              </div>
              <!-- /.card -->

            </div>
            <!--/.col (left) -->
            <!-- right column -->
            <div class="col-md-5">
              <!-- general form elements disabled -->
              <div class="card card-warning">
                <div class="card-header text-white bg-success">
                  <h3 class="card-title">Evaluation timeline</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                   <form role="form">
                    <div class="container">

                        <div class="timeline row">

                            <div class="card col-12">
                                <div class="card-body p-1">
                                    <h2>Ajouté le: <small class="text-secondary"><i class="fas fa-clock bg-gray"></i> {{$courrier->created_at}}</small></h2>
                                    <p>Création de la publication</p>
                                    <p>Par <b>{{ $courrier->user->name }}</b></p>
                                </div>
                            </div>
                            <br>
                            @foreach ($traitements as $status)

                            <div class="card col-12">
                                @if($status->user->roles()->first()->id == 1)
                                    <small class="label label-primary">Décision de l'administrateur</small>
                                @endif
                                <div class="card-body p-1">
                                    <h2>{{$status->etat->libelle}} <small class="text-secondary"><i class="fas fa-clock bg-gray"></i> {{$status->created_at}}</small></h2>
                                    <p>{{$status->commentaire}}</p>
                                    <p>Par <b>{{ $status->user->name }}</b></p>
                                </div>
                            </div>
                            @endforeach


                        </div>

                      </div>
                    </form>
                </div>
                <!-- /.card-body -->
              </div>

              </div>



    </div>
</div>



@endsection
