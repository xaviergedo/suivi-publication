@extends('layouts.admin')
@section('content')

<style>
    fieldset
    {
        border: 1px solid gray;
        -moz-border-radius:8px;
        -webkit-border-radius:8px;
        border-radius:8px;
    }
</style>

<div class="card">
    <div class="card-header">
        {{ trans('global.edit') }} publication
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.courriers.update", [$courrier->id]) }}" enctype="multipart/form-data">
            @method('PUT')
            @csrf

            <fieldset class="px-4 pb-4 mb-5 ">
                <legend class="px-3" style="color:#fff;background-color:#004f73">IDENTIFICATION PUBLICATION</legend>
                <div class="form-row">
                    {{-- <div class="col-md-6">
                        <label class="required" for="numero">{{ trans('cruds.courrier.fields.numero') }}</label>
                        <input class="form-control {{ $errors->has('numero') ? 'is-invalid' : '' }}" type="text" name="numero" id="numero" value="{{ old('numero', $courrier->numero) }}" required>
                        @if($errors->has('numero'))
                            <div class="invalid-feedback">
                                {{ $errors->first('numero') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.numero_helper') }}</span>
                    </div>
                    <div class="col-md-6">
                        <label class="required" for="numero_registre">{{ trans('cruds.courrier.fields.numero_registre') }}</label>
                        <input class="form-control {{ $errors->has('numero_registre') ? 'is-invalid' : '' }}" type="text" name="numero_registre" id="numero_registre" value="{{ old('numero_registre', $courrier->numero_registre) }}" required>
                        @if($errors->has('numero_registre'))
                            <div class="invalid-feedback">
                                {{ $errors->first('numero_registre') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.numero_registre_helper') }}</span>
                    </div> --}}
                    <div class="col-md-12">
                        <label class="required" for="objet">Titre</label>
                        <input class="form-control {{ $errors->has('objet') ? 'is-invalid' : '' }}" type="text" name="objet" id="objet" value="{{ old('objet', $courrier->objet) }}" required>
                        @if($errors->has('objet'))
                            <div class="invalid-feedback">
                                {{ $errors->first('objet') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.objet_helper') }}</span>
                    </div>

                    <div class="col-md-12">
                        <label class="required" for="references">Mots clés</label>
                        <input class="form-control {{ $errors->has('references') ? 'is-invalid' : '' }}" type="text" name="references" id="references" value="{{ old('references', $courrier->references) }}" required>
                        @if($errors->has('references'))
                            <div class="invalid-feedback">
                                {{ $errors->first('references') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.references_helper') }}</span>
                    </div>
                    <div class="col-md-4">
                        <label class="col-form-label" for="direction">Type publication</label>
                        <select id="expediteur_id" name="expediteur_id" class="form-control select2 @error('expediteur_id') is-invalid @enderror" required="required">
                            <option value="" ></option>
                            @foreach ($type_publications as $direction)
                                <option value="{{$direction->id}}"
                                {{$direction->id == $courrier->expediteur_id ? 'selected':''}}
                                >{{$direction->libelle}}</option>
                            @endforeach
                        </select>
                        @error('direction')
                        <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                        </span>
                        @enderror
                    </div>
                    <div class="col-md-8">
                        <label for="acteur">CO-auteurs</label>
                        <input class="form-control {{ $errors->has('acteur') ? 'is-invalid' : '' }}" type="text" name="acteur" id="acteur" value="{{ old('acteur', $courrier->acteur) }}">
                        @if($errors->has('acteur'))
                            <div class="invalid-feedback">
                                {{ $errors->first('acteur') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.acteur_helper') }}</span>
                    </div>
                </div>
            </fieldset>


            {{-- <fieldset class="px-4 pb-4 mb-5 ">
                <legend class="px-3" style="color:#fff;background-color:#004f73">AUTRE INFO SUR LE COURRIER</legend>
                <div class="form-row">
                    <div class="col-md-2">
                        <label class="required">{{ trans('cruds.courrier.fields.type') }}</label>
                        @foreach(App\Models\Courrier::TYPE_RADIO as $key => $label)
                            <div class="form-check {{ $errors->has('type') ? 'is-invalid' : '' }}">
                                <input class="form-check-input" type="radio" id="type_{{ $key }}" name="type" value="{{ $key }}" {{ old('type', $courrier->type) === (string) $key ? 'checked' : '' }} required>
                                <label class="form-check-label" for="type_{{ $key }}">{{ $label }}</label>
                            </div>
                        @endforeach
                        @if($errors->has('type'))
                            <div class="invalid-feedback">
                                {{ $errors->first('type') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.type_helper') }}</span>
                    </div>
                    <div class="col-md-2">
                        <label class="required" for="date_enregistree">{{ trans('cruds.courrier.fields.date_enregistree') }}</label>
                        <input class="form-control datetime {{ $errors->has('date_enregistree') ? 'is-invalid' : '' }}" type="text" name="date_enregistree" id="date_enregistree" value="{{ old('date_enregistree', $courrier->date_enregistree) }}" required>
                        @if($errors->has('date_enregistree'))
                            <div class="invalid-feedback">
                                {{ $errors->first('date_enregistree') }}
                            </div>
                        @endif
                        <span class="help-block">{{ trans('cruds.courrier.fields.date_enregistree_helper') }}</span>
                    </div>

                </div>
            </fieldset> --}}

            <div class="form-group">
                <label for="commentaire">Résumé</label>
                <textarea class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" name="commentaire" id="commentaire">{!! old('commentaire', $courrier->commentaire) !!}</textarea>
                @if($errors->has('commentaire'))
                    <div class="invalid-feedback">
                        {{ $errors->first('commentaire') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courrier.fields.commentaire_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="pieces">{{ trans('cruds.courrier.fields.pieces') }}</label>
                <div class="needsclick dropzone {{ $errors->has('pieces') ? 'is-invalid' : '' }}" id="pieces-dropzone">
                </div>
                @if($errors->has('pieces'))
                    <div class="invalid-feedback">
                        {{ $errors->first('pieces') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.courrier.fields.pieces_helper') }}</span>
            </div>
            <div class="col-md-3 mx-auto">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>

@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.courriers.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $courrier->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    var uploadedPiecesMap = {}
Dropzone.options.piecesDropzone = {
    url: '{{ route('admin.courriers.storeMedia') }}',
    maxFilesize: 5, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 5
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="pieces[]" value="' + response.name + '">')
      uploadedPiecesMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedPiecesMap[file.name]
      }
      $('form').find('input[name="pieces[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($courrier) && $courrier->pieces)
          var files =
            {!! json_encode($courrier->pieces) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="pieces[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection
