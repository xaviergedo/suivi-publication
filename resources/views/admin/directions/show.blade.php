@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.direction.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.directions.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.direction.fields.id') }}
                        </th>
                        <td>
                            {{ $direction->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.direction.fields.sigle') }}
                        </th>
                        <td>
                            {{ $direction->sigle }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.direction.fields.description') }}
                        </th>
                        <td>
                            {{ $direction->description }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.direction.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Direction::TYPE_RADIO[$direction->type] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.directions.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection