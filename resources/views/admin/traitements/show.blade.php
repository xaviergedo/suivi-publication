@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.traitement.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.traitements.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.id') }}
                        </th>
                        <td>
                            {{ $traitement->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.courrier') }}
                        </th>
                        <td>
                            {{ $traitement->courrier->numero ?? '' }}
                            {{-- @foreach($traitement->courriers as $key => $courrier)
                                <span class="label label-info">{{ $courrier->numero }}</span>
                            @endforeach --}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.direction') }}
                        </th>
                        <td>
                            {{ $traitement->direction->sigle ?? '' }}
                            {{-- @foreach($traitement->directions as $key => $direction)
                                <span class="label label-info">{{ $direction->sigle }}</span>
                            @endforeach --}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.date_traitement') }}
                        </th>
                        <td>
                            {{ $traitement->date_traitement }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.commentaire') }}
                        </th>
                        <td>
                            {!! $traitement->commentaire !!}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.etat') }}
                        </th>
                        <td>
                            {{ $traitement->etat->libelle ?? '' }}
                            {{-- @foreach($traitement->etats as $key => $etat)
                                <span class="label label-info">{{ $etat->libelle }}</span>
                            @endforeach --}}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.date_sortie') }}
                        </th>
                        <td>
                            {{ $traitement->date_sortie }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.user') }}
                        </th>
                        <td>
                            {{ $traitement->user->name ?? '' }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.traitement.fields.piece') }}
                        </th>
                        <td>
                            @foreach($traitement->piece as $key => $media)
                                <a href="{{ $media->getUrl() }}" target="_blank">
                                    {{ trans('global.view_file') }}
                                </a>
                            @endforeach
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.traitements.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection
