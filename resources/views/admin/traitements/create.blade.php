@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.create') }} {{ trans('cruds.traitement.title_singular') }}
    </div>

    <div class="card-body">
        <form method="POST" action="{{ route("admin.traitements.store") }}" enctype="multipart/form-data">
            @csrf
            <div class="form-group">
                <label for="courriers">{{ trans('cruds.traitement.fields.courrier') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('courriers') ? 'is-invalid' : '' }}" name="courriers[]" id="courriers" multiple>
                    @foreach($courriers as $id => $courrier)
                        <option value="{{ $id }}" {{ in_array($id, old('courriers', [])) ? 'selected' : '' }}>{{ $courrier }}</option>
                    @endforeach
                </select>
                @if($errors->has('courriers'))
                    <div class="invalid-feedback">
                        {{ $errors->first('courriers') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.courrier_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="directions">{{ trans('cruds.traitement.fields.direction') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('directions') ? 'is-invalid' : '' }}" name="directions[]" id="directions" multiple>
                    @foreach($directions as $id => $direction)
                        <option value="{{ $id }}" {{ in_array($id, old('directions', [])) ? 'selected' : '' }}>{{ $direction }}</option>
                    @endforeach
                </select>
                @if($errors->has('directions'))
                    <div class="invalid-feedback">
                        {{ $errors->first('directions') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.direction_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_traitement">{{ trans('cruds.traitement.fields.date_traitement') }}</label>
                <input class="form-control datetime {{ $errors->has('date_traitement') ? 'is-invalid' : '' }}" type="text" name="date_traitement" id="date_traitement" value="{{ old('date_traitement') }}">
                @if($errors->has('date_traitement'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_traitement') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.date_traitement_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="commentaire">{{ trans('cruds.traitement.fields.commentaire') }}</label>
                <textarea class="form-control ckeditor {{ $errors->has('commentaire') ? 'is-invalid' : '' }}" name="commentaire" id="commentaire">{!! old('commentaire') !!}</textarea>
                @if($errors->has('commentaire'))
                    <div class="invalid-feedback">
                        {{ $errors->first('commentaire') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.commentaire_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="etats">{{ trans('cruds.traitement.fields.etat') }}</label>
                <div style="padding-bottom: 4px">
                    <span class="btn btn-info btn-xs select-all" style="border-radius: 0">{{ trans('global.select_all') }}</span>
                    <span class="btn btn-info btn-xs deselect-all" style="border-radius: 0">{{ trans('global.deselect_all') }}</span>
                </div>
                <select class="form-control select2 {{ $errors->has('etats') ? 'is-invalid' : '' }}" name="etats[]" id="etats" multiple>
                    @foreach($etats as $id => $etat)
                        <option value="{{ $id }}" {{ in_array($id, old('etats', [])) ? 'selected' : '' }}>{{ $etat }}</option>
                    @endforeach
                </select>
                @if($errors->has('etats'))
                    <div class="invalid-feedback">
                        {{ $errors->first('etats') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.etat_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="date_sortie">{{ trans('cruds.traitement.fields.date_sortie') }}</label>
                <input class="form-control datetime {{ $errors->has('date_sortie') ? 'is-invalid' : '' }}" type="text" name="date_sortie" id="date_sortie" value="{{ old('date_sortie') }}">
                @if($errors->has('date_sortie'))
                    <div class="invalid-feedback">
                        {{ $errors->first('date_sortie') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.date_sortie_helper') }}</span>
            </div>
            <div class="form-group">
                <label for="piece">{{ trans('cruds.traitement.fields.piece') }}</label>
                <div class="needsclick dropzone {{ $errors->has('piece') ? 'is-invalid' : '' }}" id="piece-dropzone">
                </div>
                @if($errors->has('piece'))
                    <div class="invalid-feedback">
                        {{ $errors->first('piece') }}
                    </div>
                @endif
                <span class="help-block">{{ trans('cruds.traitement.fields.piece_helper') }}</span>
            </div>
            <div class="form-group">
                <button class="btn btn-danger" type="submit">
                    {{ trans('global.save') }}
                </button>
            </div>
        </form>
    </div>
</div>



@endsection

@section('scripts')
<script>
    $(document).ready(function () {
  function SimpleUploadAdapter(editor) {
    editor.plugins.get('FileRepository').createUploadAdapter = function(loader) {
      return {
        upload: function() {
          return loader.file
            .then(function (file) {
              return new Promise(function(resolve, reject) {
                // Init request
                var xhr = new XMLHttpRequest();
                xhr.open('POST', '{{ route('admin.traitements.storeCKEditorImages') }}', true);
                xhr.setRequestHeader('x-csrf-token', window._token);
                xhr.setRequestHeader('Accept', 'application/json');
                xhr.responseType = 'json';

                // Init listeners
                var genericErrorText = `Couldn't upload file: ${ file.name }.`;
                xhr.addEventListener('error', function() { reject(genericErrorText) });
                xhr.addEventListener('abort', function() { reject() });
                xhr.addEventListener('load', function() {
                  var response = xhr.response;

                  if (!response || xhr.status !== 201) {
                    return reject(response && response.message ? `${genericErrorText}\n${xhr.status} ${response.message}` : `${genericErrorText}\n ${xhr.status} ${xhr.statusText}`);
                  }

                  $('form').append('<input type="hidden" name="ck-media[]" value="' + response.id + '">');

                  resolve({ default: response.url });
                });

                if (xhr.upload) {
                  xhr.upload.addEventListener('progress', function(e) {
                    if (e.lengthComputable) {
                      loader.uploadTotal = e.total;
                      loader.uploaded = e.loaded;
                    }
                  });
                }

                // Send request
                var data = new FormData();
                data.append('upload', file);
                data.append('crud_id', '{{ $traitement->id ?? 0 }}');
                xhr.send(data);
              });
            })
        }
      };
    }
  }

  var allEditors = document.querySelectorAll('.ckeditor');
  for (var i = 0; i < allEditors.length; ++i) {
    ClassicEditor.create(
      allEditors[i], {
        extraPlugins: [SimpleUploadAdapter]
      }
    );
  }
});
</script>

<script>
    var uploadedPieceMap = {}
Dropzone.options.pieceDropzone = {
    url: '{{ route('admin.traitements.storeMedia') }}',
    maxFilesize: 3, // MB
    addRemoveLinks: true,
    headers: {
      'X-CSRF-TOKEN': "{{ csrf_token() }}"
    },
    params: {
      size: 3
    },
    success: function (file, response) {
      $('form').append('<input type="hidden" name="piece[]" value="' + response.name + '">')
      uploadedPieceMap[file.name] = response.name
    },
    removedfile: function (file) {
      file.previewElement.remove()
      var name = ''
      if (typeof file.file_name !== 'undefined') {
        name = file.file_name
      } else {
        name = uploadedPieceMap[file.name]
      }
      $('form').find('input[name="piece[]"][value="' + name + '"]').remove()
    },
    init: function () {
@if(isset($traitement) && $traitement->piece)
          var files =
            {!! json_encode($traitement->piece) !!}
              for (var i in files) {
              var file = files[i]
              this.options.addedfile.call(this, file)
              file.previewElement.classList.add('dz-complete')
              $('form').append('<input type="hidden" name="piece[]" value="' + file.file_name + '">')
            }
@endif
    },
     error: function (file, response) {
         if ($.type(response) === 'string') {
             var message = response //dropzone sends it's own error messages in string
         } else {
             var message = response.errors.file
         }
         file.previewElement.classList.add('dz-error')
         _ref = file.previewElement.querySelectorAll('[data-dz-errormessage]')
         _results = []
         for (_i = 0, _len = _ref.length; _i < _len; _i++) {
             node = _ref[_i]
             _results.push(node.textContent = message)
         }

         return _results
     }
}
</script>
@endsection