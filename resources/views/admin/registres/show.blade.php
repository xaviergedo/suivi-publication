@extends('layouts.admin')
@section('content')

<div class="card">
    <div class="card-header">
        {{ trans('global.show') }} {{ trans('cruds.registre.title') }}
    </div>

    <div class="card-body">
        <div class="form-group">
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.registres.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
            <table class="table table-bordered table-striped">
                <tbody>
                    <tr>
                        <th>
                            {{ trans('cruds.registre.fields.id') }}
                        </th>
                        <td>
                            {{ $registre->id }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.registre.fields.numero') }}
                        </th>
                        <td>
                            {{ $registre->numero }}
                        </td>
                    </tr>
                    <tr>
                        <th>
                            {{ trans('cruds.registre.fields.type') }}
                        </th>
                        <td>
                            {{ App\Models\Registre::TYPE_RADIO[$registre->type] ?? '' }}
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="form-group">
                <a class="btn btn-default" href="{{ route('admin.registres.index') }}">
                    {{ trans('global.back_to_list') }}
                </a>
            </div>
        </div>
    </div>
</div>



@endsection