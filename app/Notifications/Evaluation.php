<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class NewDossier extends Notification
{
    use Queueable;

    private $data;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                ->subject("Décision sur votre publication")
                ->greeting("Bonjour")
                ->line('Nouvelle évaluation de votre publication.')
                ->line('Par : '.$this->data['nom_prenom'])
                ->line('Email : '.$this->data['email'])
                ->line('Téléphone : '.$this->data['tel'])
                ->line('Titre : '.$this->data['titre'])
				->line('Avis :'.$this->data['avis'])
				->line('Commentaire :'.$this->data['commentaire'])
                ->salutation('Cordialement, l\'équipe.');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
