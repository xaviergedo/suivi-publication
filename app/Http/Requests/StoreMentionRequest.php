<?php

namespace App\Http\Requests;

use App\Models\Mention;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreMentionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('mention_create');
    }

    public function rules()
    {
        return [
            'libelle' => [
                'string',
                'required',
                'unique:mentions',
            ],
        ];
    }
}
