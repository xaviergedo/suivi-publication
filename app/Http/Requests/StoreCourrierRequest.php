<?php

namespace App\Http\Requests;

use App\Models\Courrier;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreCourrierRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('courrier_create');
    }

    public function rules()
    {
        return [
            'numero' => [
                'string',
                //'required',
                'unique:courriers',
            ],
            'numero_registre' => [
                'string',
                //'required',
                'unique:courriers',
            ],
            'objet' => [
                'string',
                'required',
            ],
            'references' => [
                'string',
                'required',
            ],
            'type' => [
                //'required',
            ],
            'date_enregistree' => [
               // 'required',
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
            ],
            'acteur' => [
                'string',
                'nullable',
            ],
            'pieces' => [
                'array',
                'required',
            ],
        ];
    }
}
