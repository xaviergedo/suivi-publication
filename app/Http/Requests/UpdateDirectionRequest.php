<?php

namespace App\Http\Requests;

use App\Models\Direction;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateDirectionRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('direction_edit');
    }

    public function rules()
    {
        return [
            'sigle' => [
                'string',
                'required',
            ],
            'description' => [
                'string',
                'required',
            ],
            'type' => [
                'required',
            ],
        ];
    }
}
