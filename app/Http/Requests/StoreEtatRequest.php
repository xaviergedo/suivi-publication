<?php

namespace App\Http\Requests;

use App\Models\Etat;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreEtatRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('etat_create');
    }

    public function rules()
    {
        return [
            'libelle' => [
                'string',
                'required',
                'unique:etats',
            ],
        ];
    }
}
