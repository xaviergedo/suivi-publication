<?php

namespace App\Http\Requests;

use App\Models\Affectation;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreAffectationRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('affectation_create');
    }

    public function rules()
    {
        return [
            'courriers.*' => [
                'integer',
            ],
            'courriers' => [
                'required',
                'array',
            ],
            'directions.*' => [
                'integer',
            ],
            'directions' => [
                'required',
                'array',
            ],
            'date_affectation' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'date_reception' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
        ];
    }
}
