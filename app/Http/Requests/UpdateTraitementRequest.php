<?php

namespace App\Http\Requests;

use App\Models\Traitement;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateTraitementRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('traitement_edit');
    }

    public function rules()
    {
        return [
            'courriers.*' => [
                'integer',
            ],
            'courriers' => [
                'array',
            ],
            'directions.*' => [
                'integer',
            ],
            'directions' => [
                'array',
            ],
            'date_traitement' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'etats.*' => [
                'integer',
            ],
            'etats' => [
                'array',
            ],
            'date_sortie' => [
                'date_format:' . config('panel.date_format') . ' ' . config('panel.time_format'),
                'nullable',
            ],
            'piece' => [
                'array',
            ],
        ];
    }
}
