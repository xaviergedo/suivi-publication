<?php

namespace App\Http\Requests;

use App\Models\Etat;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class UpdateEtatRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('etat_edit');
    }

    public function rules()
    {
        return [
            'libelle' => [
                'string',
                'required',
                'unique:etats,libelle,' . request()->route('etat')->id,
            ],
        ];
    }
}
