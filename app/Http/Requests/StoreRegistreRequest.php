<?php

namespace App\Http\Requests;

use App\Models\Registre;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Response;

class StoreRegistreRequest extends FormRequest
{
    public function authorize()
    {
        return Gate::allows('registre_create');
    }

    public function rules()
    {
        return [
            'numero' => [
                'string',
                'required',
            ],
        ];
    }
}
