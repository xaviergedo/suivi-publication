<?php

namespace App\Http\Requests;

use App\Models\Registre;
use Gate;
use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Response;

class MassDestroyRegistreRequest extends FormRequest
{
    public function authorize()
    {
        abort_if(Gate::denies('registre_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return true;
    }

    public function rules()
    {
        return [
            'ids'   => 'required|array',
            'ids.*' => 'exists:registres,id',
        ];
    }
}
