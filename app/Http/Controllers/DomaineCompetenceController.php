<?php

namespace App\Http\Controllers;

use App\Models\DomaineCompetence;
use App\Http\Requests\StoreDomaineCompetenceRequest;
use App\Http\Requests\UpdateDomaineCompetenceRequest;

class DomaineCompetenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreDomaineCompetenceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreDomaineCompetenceRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\DomaineCompetence  $domaineCompetence
     * @return \Illuminate\Http\Response
     */
    public function show(DomaineCompetence $domaineCompetence)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\DomaineCompetence  $domaineCompetence
     * @return \Illuminate\Http\Response
     */
    public function edit(DomaineCompetence $domaineCompetence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateDomaineCompetenceRequest  $request
     * @param  \App\Models\DomaineCompetence  $domaineCompetence
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateDomaineCompetenceRequest $request, DomaineCompetence $domaineCompetence)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\DomaineCompetence  $domaineCompetence
     * @return \Illuminate\Http\Response
     */
    public function destroy(DomaineCompetence $domaineCompetence)
    {
        //
    }
}
