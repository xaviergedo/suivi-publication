<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyDirectionRequest;
use App\Http\Requests\StoreDirectionRequest;
use App\Http\Requests\UpdateDirectionRequest;
use App\Models\Direction;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DirectionsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('direction_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $directions = Direction::all();

        return view('admin.directions.index', compact('directions'));
    }

    public function create()
    {
        abort_if(Gate::denies('direction_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.directions.create');
    }

    public function store(StoreDirectionRequest $request)
    {
        $direction = Direction::create($request->all());

        return redirect()->route('admin.directions.index');
    }

    public function edit(Direction $direction)
    {
        abort_if(Gate::denies('direction_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.directions.edit', compact('direction'));
    }

    public function update(UpdateDirectionRequest $request, Direction $direction)
    {
        $direction->update($request->all());

        return redirect()->route('admin.directions.index');
    }

    public function show(Direction $direction)
    {
        abort_if(Gate::denies('direction_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.directions.show', compact('direction'));
    }

    public function destroy(Direction $direction)
    {
        abort_if(Gate::denies('direction_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $direction->delete();

        return back();
    }

    public function massDestroy(MassDestroyDirectionRequest $request)
    {
        Direction::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
