<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyUserRequest;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Direction;
use App\Models\Role;
use App\Models\User;
use App\Models\DomaineCompetence;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class UsersController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('user_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $users = User::with(['roles', 'direction'])->get();

        $roles = Role::get();

        $directions = Direction::get();

        $domaines = DomaineCompetence::get();

        return view('admin.users.index', compact('users', 'roles', 'directions', 'domaines'));
    }

    public function create()
    {
        abort_if(Gate::denies('user_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::all()->where('id','<>','4');

        $directions = Direction::pluck('sigle', 'id')->prepend(trans('global.pleaseSelect'), '');

        $domaines = DomaineCompetence::get();

        return view('admin.users.create', compact('roles', 'directions', 'domaines'));
    }

    public function store(StoreUserRequest $request)
    {
        $user = User::create($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->domaines()->sync($request->input('domaines', []));

        return redirect()->route('admin.users.index');
    }

    public function edit(User $user)
    {
        abort_if(Gate::denies('user_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $roles = Role::pluck('title', 'id');

        $directions = Direction::pluck('sigle', 'id')->prepend(trans('global.pleaseSelect'), '');

        $user->load('roles', 'direction');

        $domaines = DomaineCompetence::get();

        return view('admin.users.edit', compact('roles', 'directions', 'user', 'domaines'));
    }

    public function update(UpdateUserRequest $request, User $user)
    {
        $user->update($request->all());
        $user->roles()->sync($request->input('roles', []));
        $user->domaines()->sync($request->input('domaines', []));

        return redirect()->route('admin.users.index');
    }

    public function show(User $user)
    {
        abort_if(Gate::denies('user_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->load('roles', 'direction');

        $domaines = DomaineCompetence::get();

        return view('admin.users.show', compact('user', 'domaines'));
    }

    public function destroy(User $user)
    {
        abort_if(Gate::denies('user_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $user->delete();

        return back();
    }

    public function massDestroy(MassDestroyUserRequest $request)
    {
        User::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
