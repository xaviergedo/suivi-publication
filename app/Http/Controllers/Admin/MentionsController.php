<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyMentionRequest;
use App\Http\Requests\StoreMentionRequest;
use App\Http\Requests\UpdateMentionRequest;
use App\Models\Mention;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class MentionsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('mention_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mentions = Mention::all();

        return view('admin.mentions.index', compact('mentions'));
    }

    public function create()
    {
        abort_if(Gate::denies('mention_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mentions.create');
    }

    public function store(StoreMentionRequest $request)
    {
        $mention = Mention::create($request->all());

        return redirect()->route('admin.mentions.index');
    }

    public function edit(Mention $mention)
    {
        abort_if(Gate::denies('mention_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mentions.edit', compact('mention'));
    }

    public function update(UpdateMentionRequest $request, Mention $mention)
    {
        $mention->update($request->all());

        return redirect()->route('admin.mentions.index');
    }

    public function show(Mention $mention)
    {
        abort_if(Gate::denies('mention_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.mentions.show', compact('mention'));
    }

    public function destroy(Mention $mention)
    {
        abort_if(Gate::denies('mention_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $mention->delete();

        return back();
    }

    public function massDestroy(MassDestroyMentionRequest $request)
    {
        Mention::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
