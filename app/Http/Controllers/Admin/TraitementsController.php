<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyTraitementRequest;
use App\Http\Requests\StoreTraitementRequest;
use App\Http\Requests\UpdateTraitementRequest;
use App\Models\Courrier;
use App\Models\Direction;
use App\Models\Etat;
use App\Models\Traitement;
use App\Models\User;
use Gate;
use Auth;
use DB;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class TraitementsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('traitement_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles()->first()->id == 1){
            $traitements = Traitement::with(['courriers', 'users', 'etats', 'user', 'media'])->get();
        }else if(Auth::user()->roles()->first()->id == 3){
            $traitements = Traitement::with(['courriers', 'users', 'etats', 'user', 'media'])->where('user_id',Auth::id())->get();
        }


        $courriers = Courrier::get();

        $users = Direction::get();

        $etats = Etat::get();

        $users = User::get();

        return view('admin.traitements.index', compact('traitements', 'courriers', 'users', 'etats', 'users'));
    }

    public function create()
    {
        abort_if(Gate::denies('traitement_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courriers = Courrier::pluck('numero', 'id');

        $users = Direction::pluck('sigle', 'id');

        $etats = Etat::pluck('libelle', 'id');

        return view('admin.traitements.create', compact('courriers', 'users', 'etats'));
    }

    // public function store(StoreTraitementRequest $request)
    // {
    //     $traitement = Traitement::create($request->all());
    //     $traitement->courriers()->sync($request->input('courriers', []));
    //     $traitement->users()->sync($request->input('users', []));
    //     $traitement->etats()->sync($request->input('etats', []));
    //     foreach ($request->input('piece', []) as $file) {
    //         $traitement->addMedia(storage_path('tmp/uploads/' . basename($file)))->toMediaCollection('piece');
    //     }

    //     if ($media = $request->input('ck-media', false)) {
    //         Media::whereIn('id', $media)->update(['model_id' => $traitement->id]);
    //     }
    //     $traitement->user_id = Auth::id();
    //     $traitement->courrier_id = $request->courrier_id;
    //     $traitement->etat_id = $request->etat_id;
    //     $traitement->save();

    //     return redirect()->route('admin.traitements.index');
    // }

    public function storeTraitement(Request $request)
    {
        $traitement = Traitement::create($request->all());
        $traitement->courriers()->sync($request->input('courriers', []));
        $traitement->users()->sync($request->input('users', []));
        $traitement->etats()->sync($request->input('etats', []));
        $traitement->date_traitement = $request->date_traitement;
        if(Auth::user()->roles()->first()->id == 1){
            $affectation_id = $request->courrier_id ;

            $affectation_courriers = Courrier::select('courriers.*')
            ->join('affectation_courrier', 'courriers.id', 'affectation_courrier.courrier_id')
            ->where(['affectation_courrier.affectation_id' =>$request->courrier_id ])->first();

            $courrier_id = $affectation_courriers->id;

            $traitement->courrier_id = $courrier_id;
        }else {
            $courrier_id = $request->courrier_id;
            $traitement->courrier_id = $request->courrier_id;
        }
        $traitement->etat_id = $request->etat_id;
        $traitement->commentaire = $request->commentaire;
        $traitement->user_id = Auth::id();;
        $traitement->save();
        foreach ($request->input('piece', []) as $file) {
            $traitement->addMedia(storage_path('tmp/uploads/' . basename($file)))->toMediaCollection('piece');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $traitement->id]);
        }

        $courriers = Courrier::select('courriers.*')
            ->where(['id' =>$courrier_id])->first();

        $data = [
            'pub_id' => $courriers->id,
            'nom_prenom' => $courriers->user->name,
            'email' => $courriers->user->email,
            'tel' => $courriers->user->tel,
            'titre' => $courriers->objet,
            'avis' => $traitement->etat->libelle,
            'commentaire' => $traitement->commentaire,
        ];

        $users = ['ssoutienpme@finances.bj'];
        Notification::route('mail', $users)->notify(new Evaluation($data));

        return redirect()->route('admin.traitements.index');
    }

    public function edit(Traitement $traitement)
    {
        abort_if(Gate::denies('traitement_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courriers = Courrier::pluck('numero', 'id');

        $users = Direction::pluck('sigle', 'id');

        $etats = Etat::all();

        $traitement->load('courriers', 'users', 'etats', 'user');

        return view('admin.traitements.edit', compact('courriers', 'users', 'etats', 'traitement'));
    }

    public function update(UpdateTraitementRequest $request, Traitement $traitement)
    {
        $traitement->update($request->all());
        $traitement->courriers()->sync($request->input('courriers', []));
        $traitement->users()->sync($request->input('users', []));
        $traitement->etats()->sync($request->input('etats', []));
        if (count($traitement->piece) > 0) {
            foreach ($traitement->piece as $media) {
                if (!in_array($media->file_name, $request->input('piece', []))) {
                    $media->delete();
                }
            }
        }
        $media = $traitement->piece->pluck('file_name')->toArray();
        foreach ($request->input('piece', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $traitement->addMedia(storage_path('tmp/uploads/' . basename($file)))->toMediaCollection('piece');
            }
        }
        $traitement->date_traitement = $request->date_traitement;
        // $traitement->courrier_id = $request->courrier_id;
        $traitement->etat_id = $request->etat_id;
        $traitement->date_sortie = $request->date_sortie;
        $traitement->commentaire = $request->commentaire;
        $traitement->user_id = Auth::id();;
        $traitement->direction_id = Auth::user()->direction_id;
        $traitement->save();

        return redirect()->route('admin.traitements.index');
    }

    public function show(Traitement $traitement)
    {
        abort_if(Gate::denies('traitement_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $traitement->load('courriers', 'users', 'etats', 'user');

        return view('admin.traitements.show', compact('traitement'));
    }

    public function destroy(Traitement $traitement)
    {
        abort_if(Gate::denies('traitement_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $traitement->delete();

        return back();
    }

    public function massDestroy(MassDestroyTraitementRequest $request)
    {
        Traitement::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('traitement_create') && Gate::denies('traitement_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Traitement();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
