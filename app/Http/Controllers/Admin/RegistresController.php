<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Registre;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class RegistresController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('registre_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $registres = Registre::all();

        return view('admin.registres.index', compact('registres'));
    }

    public function show(Registre $registre)
    {
        abort_if(Gate::denies('registre_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.registres.show', compact('registre'));
    }
}
