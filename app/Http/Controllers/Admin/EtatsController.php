<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\MassDestroyEtatRequest;
use App\Http\Requests\StoreEtatRequest;
use App\Http\Requests\UpdateEtatRequest;
use App\Models\Etat;
use Gate;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class EtatsController extends Controller
{
    public function index()
    {
        abort_if(Gate::denies('etat_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $etats = Etat::all();

        return view('admin.etats.index', compact('etats'));
    }

    public function create()
    {
        abort_if(Gate::denies('etat_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.etats.create');
    }

    public function store(StoreEtatRequest $request)
    {
        $etat = Etat::create($request->all());

        return redirect()->route('admin.etats.index');
    }

    public function edit(Etat $etat)
    {
        abort_if(Gate::denies('etat_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.etats.edit', compact('etat'));
    }

    public function update(UpdateEtatRequest $request, Etat $etat)
    {
        $etat->update($request->all());

        return redirect()->route('admin.etats.index');
    }

    public function show(Etat $etat)
    {
        abort_if(Gate::denies('etat_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        return view('admin.etats.show', compact('etat'));
    }

    public function destroy(Etat $etat)
    {
        abort_if(Gate::denies('etat_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $etat->delete();

        return back();
    }

    public function massDestroy(MassDestroyEtatRequest $request)
    {
        Etat::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }
}
