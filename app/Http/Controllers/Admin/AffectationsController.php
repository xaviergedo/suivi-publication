<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyAffectationRequest;
use App\Http\Requests\StoreAffectationRequest;
use App\Http\Requests\UpdateAffectationRequest;
use App\Models\Affectation;
use App\Models\Courrier;
use App\Models\Direction;
use App\Models\User;
use App\Models\Etat;
use Gate;
use Auth;
use DB;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;

class AffectationsController extends Controller
{
    use MediaUploadingTrait;

    public function index()
    {
        abort_if(Gate::denies('affectation_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $affectations = Affectation::with(['courriers', 'users', 'user'])->get();

        $users = User::get();

        $etats = Etat::get();

        $affectation_courriers = Courrier::select('courriers.*')
        ->join('affectation_user', 'courriers.id', 'affectation_user.courrier_id')
        ->where(['affectation_user.user_id' =>Auth::id()])->get();

        $s_affectation_courriers = Courrier::select('courriers.*')
        ->join('affectation_user', 'courriers.id', 'affectation_user.courrier_id')
        ->where(['affectation_user.user_id' =>Auth::id()])->first();

        if($s_affectation_courriers){
            $traitement_by_relecteur = DB::table('traitements')->where(['courrier_id'=> $s_affectation_courriers->id,'user_id'=>Auth::id()])->get()->count();
        }else {
            $traitement_by_relecteur ="";
        }
        return view('admin.affectations.index', compact('affectations', 'users', 'etats', 'affectation_courriers','traitement_by_relecteur','s_affectation_courriers'));
    }

    public function create()
    {
        abort_if(Gate::denies('affectation_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courriers = Courrier::pluck('numero', 'id');

        $directions = Direction::pluck('sigle', 'id');

        return view('admin.affectations.create', compact('courriers', 'directions'));
    }

    // public function store(StoreAffectationRequest $request)
    // {
    //     //dd('ok');
    //     $request->validate([
    //         "date_affectation"      => "datetime|required",
    //     ]);

    //     $affectation = Affectation::create($request->all());
    //     $affectation->courriers()->sync($request->input('courriers', []));
    //     $affectation->users()->sync($request->input('users', []));
    //     $cc = $request->input('users', []);
    //     $affectation->direction_lead_id = $cc[0];
    //     //dd($cc[0]);
    //     if ($media = $request->input('ck-media', false)) {
    //         Media::whereIn('id', $media)->update(['model_id' => $affectation->id]);
    //     }

    //     return redirect()->route('admin.affectations.index');
    // }

    public function storeAffectation(Request $request)
    {
        $request->validate([
            "date_affectation"      => "date|required",
        ]);

        $affectation = Affectation::create($request->all());
        $affectation->courriers()->sync($request->courrier_id);
        $affectation->users()->sync($request->input('users', []));
        // $cc = $request->input('users', []);
        // $affectation->direction_lead_id =  $cc[array_key_first($cc)];
        $affectation->user_id = Auth::id();
        $affectation->save();
        $affectation_update = DB::table('affectation_user')->where('affectation_id' , $affectation->id)->update(['courrier_id' => $request->courrier_id]);
        $affectation_update_courrier = DB::table('courriers')->where('id' , $request->courrier_id)->update(['affectation_id' => $affectation->id]);
        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $affectation->id]);
        }

        return redirect()->route('admin.affectations.index');
    }

    public function storeReception(Request $request)
    {
        $request->validate([
            "date_reception"      => "date|required",
        ]);
        $user_id = Auth::user()->user_id;
        $affectation_id = DB::table('affectation_courrier')->where('courrier_id', $request->courrier_id)->get('affectation_id')->first();
        $affectation_update_reception = DB::table('affectation_user')->where(['user_id' =>$user_id, 'affectation_id' =>$affectation_id->affectation_id])->update(['date_reception' => date('Y-m-d H:i:s', strtotime($request->date_reception)) , 'commentaire' =>$request->commentaire]);

        return redirect()->route('admin.affectations.index');
    }

    public function edit(Affectation $affectation)
    {
        abort_if(Gate::denies('affectation_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courriers = Courrier::pluck('numero', 'id');

        $directions = Direction::pluck('sigle', 'id');

        $affectation->load('courriers', 'directions', 'user', 'direction_lead');

        return view('admin.affectations.edit', compact('courriers', 'directions', 'affectation'));
    }

    public function update(UpdateAffectationRequest $request, Affectation $affectation)
    {
        $request->validate([
            "date_affectation"      => "date|required",
        ]);
        $affectation->update($request->all());
        $affectation->courriers()->sync($request->courrier_id);
        $affectation->usesr()->sync($request->input('usesr', []));
        // $cc = $request->input('directions', []);
        // $affectation->direction_lead_id =  $cc[array_key_first($cc)];
        $affectation->user_id = Auth::id();
        $affectation->save();

        return redirect()->route('admin.affectations.index');
    }

    public function show(Affectation $affectation)
    {
        abort_if(Gate::denies('affectation_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $affectation->load('courriers', 'users', 'user', 'direction_lead');

        return view('admin.affectations.show', compact('affectation'));
    }

    public function destroy(Affectation $affectation)
    {
        abort_if(Gate::denies('affectation_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $affectation->delete();

        $affectation_user = DB::table('affectation_user')->where('affectation_id', $affectation->id)->delete();
        $affectation_courrier = DB::table('affectation_courrier')->where('affectation_id', $affectation->id)->delete();

        return back();
    }

    public function massDestroy(MassDestroyAffectationRequest $request)
    {
        Affectation::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('affectation_create') && Gate::denies('affectation_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Affectation();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
