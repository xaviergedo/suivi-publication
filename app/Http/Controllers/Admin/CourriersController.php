<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Traits\MediaUploadingTrait;
use App\Http\Requests\MassDestroyCourrierRequest;
use App\Http\Requests\StoreCourrierRequest;
use App\Http\Requests\UpdateCourrierRequest;
use App\Models\Courrier;
use App\Models\Mention;
use App\Models\Registre;
use App\Models\User;
use App\Models\Direction;
use App\Models\TypePublication;
use App\Models\Traitement;
use App\Models\Etat;
use App\Models\Role;
use Gate;
use Auth;
use DB;
use Illuminate\Http\Request;
use Spatie\MediaLibrary\MediaCollections\Models\Media;
use Symfony\Component\HttpFoundation\Response;
use App\Traits\TouteFonction;

class CourriersController extends Controller
{
    use MediaUploadingTrait;
    use TouteFonction;

    public function index()
    {
        // abort_if(Gate::denies('courrier_access'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        if(Auth::user()->roles()->first()->id == 1){
            $courriers = Courrier::with(['user', 'mention', 'registre', 'media'])->get();
        }elseif (Auth::user()->roles()->first()->id == 3) {
            $courriers = Courrier::with(['user', 'mention', 'registre', 'media'])
            ->join('affectation_direction', 'courriers.id', 'affectation_direction.courrier_id')
            ->where(['direction_id' =>Auth::user()->direction_id])->get();
        }elseif (Auth::user()->roles()->first()->id == 4) {
            $courriers = Courrier::with(['user', 'mention', 'registre', 'media'])->where('user_id',Auth::id())->get();
        }elseif (Auth::user()->roles()->first()->id == 5) {
            $courriers = Courrier::with(['user', 'mention', 'registre', 'media'])->get();
        }
        // $courriers = Courrier::get();

        $users = User::whereHas('roles', function($role) {
            $role->where('title', '=', 'Relecteur');
        })->get();

        $mentions = Mention::get();

        $registres = Registre::get();

        $directions = Direction::get();

        $etats = Etat::get();

        return view('admin.courriers.index', compact('courriers', 'users', 'mentions', 'registres', 'directions', 'etats'));
    }

    public function create()
    {
        abort_if(Gate::denies('courrier_create'), Response::HTTP_FORBIDDEN, '403 Forbidden');
        // $courrier_all = Courrier::all()->count() + 1;
        // if(Auth::user()->roles()->first()->id == 4) {
        //     if(Auth::user()->direction_id == 7){ // SP
        //         $num_courrier = $courrier_all."/SP" ;
        //     }
        //     if(Auth::user()->direction_id == 10){ // SECRETARIAT
        //         $num_courrier = $courrier_all."/SG";
        //     }
        // }
        $num_courrier = "";
        $directionns = Direction::get();
        $type_publications = TypePublication::get();

        return view('admin.courriers.create', compact('num_courrier' , 'directionns','type_publications'));
    }

    public function store(StoreCourrierRequest $request)
    {

        $request->validate([
           // 'pieces'   => "required|mimes:doc,docx,pdf|max:5120",
        ]);
        $courrier = Courrier::create($request->all());

        foreach ($request->input('pieces', []) as $file) {
            $courrier->addMedia(storage_path('tmp/uploads/' . basename($file)))->toMediaCollection('pieces');
        }

        if ($media = $request->input('ck-media', false)) {
            Media::whereIn('id', $media)->update(['model_id' => $courrier->id]);
        }
        $courrier->user_id = Auth::id();
        $courrier->type_publication_id = $request->expediteur_id;
        $courrier->save();

        return redirect()->route('admin.courriers.index');
    }

    public function edit(Courrier $courrier)
    {
        abort_if(Gate::denies('courrier_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courrier->load('user', 'mention', 'registre');

        $directionns = Direction::get();

        $type_publications = TypePublication::get();

        return view('admin.courriers.edit', compact('courrier' , 'directionns', 'type_publications'));
    }

    public function update(UpdateCourrierRequest $request, Courrier $courrier)
    {
        $courrier->update($request->all());

        if (count($courrier->pieces) > 0) {
            foreach ($courrier->pieces as $media) {
                if (!in_array($media->file_name, $request->input('pieces', []))) {
                    $media->delete();
                }
            }
        }
        $media = $courrier->pieces->pluck('file_name')->toArray();
        foreach ($request->input('pieces', []) as $file) {
            if (count($media) === 0 || !in_array($file, $media)) {
                $courrier->addMedia(storage_path('tmp/uploads/' . basename($file)))->toMediaCollection('pieces');
            }
        }
        $courrier->type_publication_id = $request->expediteur_id;
        $courrier->save();

        return redirect()->route('admin.courriers.index');
    }

    public function show(Courrier $courrier)
    {
        abort_if(Gate::denies('courrier_show'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courrier->load('user', 'mention', 'registre');

        $traitements = Traitement::with(['courriers', 'users', 'etats', 'user', 'media'])->where('courrier_id',$courrier->id)->get();

        return view('admin.courriers.show', compact('courrier','traitements'));
    }

    public function destroy(Courrier $courrier)
    {
        abort_if(Gate::denies('courrier_delete'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $courrier->delete();

        return back();
    }

    public function massDestroy(MassDestroyCourrierRequest $request)
    {
        Courrier::whereIn('id', request('ids'))->delete();

        return response(null, Response::HTTP_NO_CONTENT);
    }

    public function storeCKEditorImages(Request $request)
    {
        abort_if(Gate::denies('courrier_create') && Gate::denies('courrier_edit'), Response::HTTP_FORBIDDEN, '403 Forbidden');

        $model         = new Courrier();
        $model->id     = $request->input('crud_id', 0);
        $model->exists = true;
        $media         = $model->addMediaFromRequest('upload')->toMediaCollection('ck-media');

        return response()->json(['id' => $media->id, 'url' => $media->getUrl()], Response::HTTP_CREATED);
    }
}
