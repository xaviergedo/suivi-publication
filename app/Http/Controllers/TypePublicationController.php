<?php

namespace App\Http\Controllers;

use App\Models\TypePublication;
use App\Http\Requests\StoreTypePublicationRequest;
use App\Http\Requests\UpdateTypePublicationRequest;

class TypePublicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreTypePublicationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTypePublicationRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\TypePublication  $typePublication
     * @return \Illuminate\Http\Response
     */
    public function show(TypePublication $typePublication)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\TypePublication  $typePublication
     * @return \Illuminate\Http\Response
     */
    public function edit(TypePublication $typePublication)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateTypePublicationRequest  $request
     * @param  \App\Models\TypePublication  $typePublication
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTypePublicationRequest $request, TypePublication $typePublication)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\TypePublication  $typePublication
     * @return \Illuminate\Http\Response
     */
    public function destroy(TypePublication $typePublication)
    {
        //
    }
}
