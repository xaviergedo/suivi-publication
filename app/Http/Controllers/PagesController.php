<?php

namespace App\Http\Controllers;
use App\Models\DomaineCompetence;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use App\Notifications\NewUser;
use Auth;
use Session;
use Notification;
use Mail;
use Illuminate\Http\Request;
use App\Jobs\EnvoieMail;
use Illuminate\Foundation\Bus\Dispatchable;

class PagesController extends Controller
{

    public function inscription_auteur()
    {
        return view('auteur');
    }

    public function store_auteur(Request $request)
    {
        $request->validate([
            "nom"                   => "string|required",
            "prenom"                => "string|required",
            "genre"                 => "required",
            "telephone"             => "string|required",
            "email"                 => "required|email|unique:users",
            "password"              => "string|required",
        ]);

        // $send_mail = true;
        $user = User::where('email', '=', $request->email)->first();
        if (!$user) {
            // creation user
            // $random_password = rand(100000, 999999);
            $user = user::create([
                'nom' => $request->nom,
                'prenom' => $request->prenom,
                'genre' => $request->genre,
                'telephone' => $request->telephone,
                'email' => $request->email,
                'name' => $request->nom . " " . $request->prenom,
                'password' => $request->password,
            ]);
            $user->roles()->sync([4]); //auteur

            // if ($send_mail) {
            //  $data = [
            //      "id" => $user->id,
            //      "user" => $user->name,
            //      "adresse" => $user->email,
            //      "password" => $random_password
            //  ];
            //  $user->notify(new NewUser($data));
            // $this->dispatch(new EnvoieMail($user));
            // }
            Session::flash('success', "L'auteur a été créé avec succès.");
        }

        return redirect()->route("login");
    }
}
