<?php

namespace App\Models;

use \DateTimeInterface;
use App\Traits\Auditable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Traitement extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;
    use Auditable;
    use HasFactory;

    public $table = 'traitements';

    protected $appends = [
        'piece',
    ];

    protected $dates = [
        'date_traitement',
        'date_sortie',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'date_traitement',
        'commentaire',
        'date_sortie',
        'user_id',
        'courrier_id',
        'direction_id',
        'etat_id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function courriers()
    {
        return $this->belongsToMany(Courrier::class);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    public function getDateTraitementAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDateTraitementAttribute($value)
    {
        $this->attributes['date_traitement'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function etats()
    {
        return $this->belongsToMany(Etat::class);
    }

    public function getDateSortieAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDateSortieAttribute($value)
    {
        $this->attributes['date_sortie'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function direction()
    {
        return $this->belongsTo(Direction::class, 'direction_id');
    }

    public function courrier()
    {
        return $this->belongsTo(Courrier::class, 'courrier_id');
    }

    public function etat()
    {
        return $this->belongsTo(Etat::class, 'etat_id');
    }

    public function getPieceAttribute()
    {
        return $this->getMedia('piece');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
