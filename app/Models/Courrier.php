<?php

namespace App\Models;

use \DateTimeInterface;
use App\Traits\Auditable;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;
use Spatie\MediaLibrary\MediaCollections\Models\Media;

class Courrier extends Model implements HasMedia
{
    use SoftDeletes;
    use InteractsWithMedia;
    use Auditable;
    use HasFactory;

    public const ARCHIVER_RADIO = [
        '0' => 'Oui',
        '1' => 'Non',
    ];

    public const TYPE_RADIO = [
        '0' => 'Arrivé',
        '1' => 'Départ',
    ];

    public $table = 'courriers';

    protected $appends = [
        'pieces',
    ];

    protected $dates = [
        'date_enregistree',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    protected $fillable = [
        'numero',
        'numero_registre',
        'objet',
        'references',
        'type',
        'date_enregistree',
        'acteur',
        'user_id',
        'commentaire',
        'mention_id',
        'archiver',
        'registre_id',
        'expediteur_id',
        'affectation_id',
        'last_etat',
        'created_at',
        'updated_at',
        'deleted_at',
        'type_publication_id',
    ];

    public function registerMediaConversions(Media $media = null): void
    {
        $this->addMediaConversion('thumb')->fit('crop', 50, 50);
        $this->addMediaConversion('preview')->fit('crop', 120, 120);
    }

    public function getDateEnregistreeAttribute($value)
    {
        return $value ? Carbon::createFromFormat('Y-m-d H:i:s', $value)->format(config('panel.date_format') . ' ' . config('panel.time_format')) : null;
    }

    public function setDateEnregistreeAttribute($value)
    {
        $this->attributes['date_enregistree'] = $value ? Carbon::createFromFormat(config('panel.date_format') . ' ' . config('panel.time_format'), $value)->format('Y-m-d H:i:s') : null;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function mention()
    {
        return $this->belongsTo(Mention::class, 'mention_id');
    }

    public function getPiecesAttribute()
    {
        return $this->getMedia('pieces');
    }

    public function registre()
    {
        return $this->belongsTo(Registre::class, 'registre_id');
    }

    public function affectation()
    {
        return $this->belongsTo(Affectation::class, 'affectation_id');
    }

    public function expediteur()
    {
        return $this->belongsTo(Direction::class, 'expediteur_id');
    }

    public function type_publication()
    {
        return $this->belongsTo(TypePublication::class, 'type_publication_id');
    }

    protected function serializeDate(DateTimeInterface $date)
    {
        return $date->format('Y-m-d H:i:s');
    }
}
