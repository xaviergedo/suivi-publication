<?php

namespace App\Traits;

use App\Models\AuditLog;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Models\Courrier;
use App\Models\Role;
use App\Models\User;
use Auth;
use DB;

trait TouteFonction
{

    public function Courrier_Affecter(Request $request, $direction_id, $courrier_id)
    {

        $affectation_id = DB::table('affectation_courrier')->where('courrier_id', $request->courrier_id)->get('affectation_id')->first();

        $recherche = DB::table('affectation_direction')->where(['direction_id' =>$direction_id, 'affectation_id' =>$affectation_id->affectation_id])->get()->count();

        return $recherche;
    }

    public function Verifier_Courrier_Affecter(Request $request, $courrier_id)
    {

        $recherche = DB::table('affectation_direction')->where('courrier_id', $request->courrier_id)->get()->count();

        return $recherche;
    }

}
