<?php

Route::redirect('/', '/login');
Route::get('/home', function () {
    if (session('status')) {
        return redirect()->route('admin.home')->with('status', session('status'));
    }

    return redirect()->route('admin.home');
});

Auth::routes(['register' => false]);

Route::get('/inscrire-auteur', 'PagesController@inscription_auteur')->name('inscrire-auteur');
Route::post('/store-auteur', ['as' => 'store-auteur',   'uses' => 'PagesController@store_auteur']);

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => ['auth']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    // Permissions
    Route::delete('permissions/destroy', 'PermissionsController@massDestroy')->name('permissions.massDestroy');
    Route::resource('permissions', 'PermissionsController');

    // Roles
    Route::delete('roles/destroy', 'RolesController@massDestroy')->name('roles.massDestroy');
    Route::resource('roles', 'RolesController');

    // Users
    Route::delete('users/destroy', 'UsersController@massDestroy')->name('users.massDestroy');
    Route::resource('users', 'UsersController');

    // Directions
    Route::delete('directions/destroy', 'DirectionsController@massDestroy')->name('directions.massDestroy');
    Route::resource('directions', 'DirectionsController');

    // Mentions
    Route::delete('mentions/destroy', 'MentionsController@massDestroy')->name('mentions.massDestroy');
    Route::resource('mentions', 'MentionsController');

    // Registres
    Route::resource('registres', 'RegistresController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Courriers
    Route::delete('courriers/destroy', 'CourriersController@massDestroy')->name('courriers.massDestroy');
    Route::post('courriers/media', 'CourriersController@storeMedia')->name('courriers.storeMedia');
    Route::post('courriers/ckmedia', 'CourriersController@storeCKEditorImages')->name('courriers.storeCKEditorImages');
    Route::resource('courriers', 'CourriersController');
    Route::post('courriers/affecter/{id}',['as'=>'courriers.affecter',   'uses'=>'AffectationsController@storeAffectation']);
    Route::post('courriers/receptionner/{id}',['as'=>'courriers.receptionner',   'uses'=>'AffectationsController@storeReception']);
    Route::post('courriers/traiter/{id}',['as'=>'courriers.traiter',   'uses'=>'TraitementsController@storeTraitement']);
    Route::post('courriers/mes-courriers',['as'=>'courriers.mes_courriers',   'uses'=>'CourriersController@mesCourriers']);

    // Affectations
    Route::delete('affectations/destroy', 'AffectationsController@massDestroy')->name('affectations.massDestroy');
    Route::post('affectations/media', 'AffectationsController@storeMedia')->name('affectations.storeMedia');
    Route::post('affectations/ckmedia', 'AffectationsController@storeCKEditorImages')->name('affectations.storeCKEditorImages');
    Route::resource('affectations', 'AffectationsController');

    // Traitements
    Route::delete('traitements/destroy', 'TraitementsController@massDestroy')->name('traitements.massDestroy');
    Route::post('traitements/media', 'TraitementsController@storeMedia')->name('traitements.storeMedia');
    Route::post('traitements/ckmedia', 'TraitementsController@storeCKEditorImages')->name('traitements.storeCKEditorImages');
    Route::resource('traitements', 'TraitementsController');

    // Audit Logs
    Route::resource('audit-logs', 'AuditLogsController', ['except' => ['create', 'store', 'edit', 'update', 'destroy']]);

    // Faq Category
    Route::delete('faq-categories/destroy', 'FaqCategoryController@massDestroy')->name('faq-categories.massDestroy');
    Route::resource('faq-categories', 'FaqCategoryController');

    // Faq Question
    Route::delete('faq-questions/destroy', 'FaqQuestionController@massDestroy')->name('faq-questions.massDestroy');
    Route::resource('faq-questions', 'FaqQuestionController');

    // Etats
    Route::delete('etats/destroy', 'EtatsController@massDestroy')->name('etats.massDestroy');
    Route::resource('etats', 'EtatsController');

    Route::get('messenger', 'MessengerController@index')->name('messenger.index');
    Route::get('messenger/create', 'MessengerController@createTopic')->name('messenger.createTopic');
    Route::post('messenger', 'MessengerController@storeTopic')->name('messenger.storeTopic');
    Route::get('messenger/inbox', 'MessengerController@showInbox')->name('messenger.showInbox');
    Route::get('messenger/outbox', 'MessengerController@showOutbox')->name('messenger.showOutbox');
    Route::get('messenger/{topic}', 'MessengerController@showMessages')->name('messenger.showMessages');
    Route::delete('messenger/{topic}', 'MessengerController@destroyTopic')->name('messenger.destroyTopic');
    Route::post('messenger/{topic}/reply', 'MessengerController@replyToTopic')->name('messenger.reply');
    Route::get('messenger/{topic}/reply', 'MessengerController@showReply')->name('messenger.showReply');
});
Route::group(['prefix' => 'profile', 'as' => 'profile.', 'namespace' => 'Auth', 'middleware' => ['auth']], function () {
    // Change password
    if (file_exists(app_path('Http/Controllers/Auth/ChangePasswordController.php'))) {
        Route::get('password', 'ChangePasswordController@edit')->name('password.edit');
        Route::post('password', 'ChangePasswordController@update')->name('password.update');
        Route::post('profile', 'ChangePasswordController@updateProfile')->name('password.updateProfile');
        Route::post('profile/destroy', 'ChangePasswordController@destroy')->name('password.destroyProfile');
    }
});
