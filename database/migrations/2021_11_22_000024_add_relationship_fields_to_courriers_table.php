<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToCourriersTable extends Migration
{
    public function up()
    {
        Schema::table('courriers', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5364737')->references('id')->on('users');
            $table->unsignedBigInteger('mention_id')->nullable();
            $table->foreign('mention_id', 'mention_fk_5364738')->references('id')->on('mentions');
            $table->unsignedBigInteger('registre_id')->nullable();
            $table->foreign('registre_id', 'registre_fk_5364736')->references('id')->on('registres');
        });
    }
}
