<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateEtatTraitementPivotTable extends Migration
{
    public function up()
    {
        Schema::create('etat_traitement', function (Blueprint $table) {
            $table->unsignedBigInteger('traitement_id');
            $table->foreign('traitement_id', 'traitement_id_fk_5373611')->references('id')->on('traitements')->onDelete('cascade');
            $table->unsignedBigInteger('etat_id');
            $table->foreign('etat_id', 'etat_id_fk_5373611')->references('id')->on('etats')->onDelete('cascade');
        });
    }
}
