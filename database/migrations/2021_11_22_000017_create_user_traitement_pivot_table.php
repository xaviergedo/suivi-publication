<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserTraitementPivotTable extends Migration
{
    public function up()
    {
        Schema::create('traitement_user', function (Blueprint $table) {
            $table->unsignedBigInteger('traitement_id');
            $table->foreign('traitement_id', 'traitement_id_fk_5364788')->references('id')->on('traitements')->onDelete('cascade');
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_id_fk_5364788')->references('id')->on('users')->onDelete('cascade');
        });
    }
}
