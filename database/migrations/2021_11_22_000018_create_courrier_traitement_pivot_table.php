<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourrierTraitementPivotTable extends Migration
{
    public function up()
    {
        Schema::create('courrier_traitement', function (Blueprint $table) {
            $table->unsignedBigInteger('traitement_id');
            $table->foreign('traitement_id', 'traitement_id_fk_5364787')->references('id')->on('traitements')->onDelete('cascade');
            $table->unsignedBigInteger('courrier_id');
            $table->foreign('courrier_id', 'courrier_id_fk_5364787')->references('id')->on('courriers')->onDelete('cascade');
        });
    }
}
