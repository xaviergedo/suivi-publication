<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRegistresTable extends Migration
{
    public function up()
    {
        Schema::create('registres', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero');
            $table->string('type')->nullable();
            $table->timestamps();
        });
    }
}
