<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffectationsTable extends Migration
{
    public function up()
    {
        Schema::create('affectations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('date_affectation')->nullable();
            $table->datetime('date_reception')->nullable();
            $table->longText('commentaire')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
