<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddRelationshipFieldsToAffectationsTable extends Migration
{
    public function up()
    {
        Schema::table('affectations', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id')->nullable();
            $table->foreign('user_id', 'user_fk_5367194')->references('id')->on('users');
            $table->unsignedBigInteger('direction_lead_id')->nullable();
            $table->foreign('direction_lead_id', 'direction_lead_fk_5391474')->references('id')->on('directions');
        });
    }
}
