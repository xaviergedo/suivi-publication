<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTraitementsTable extends Migration
{
    public function up()
    {
        Schema::create('traitements', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->datetime('date_traitement')->nullable();
            $table->longText('commentaire')->nullable();
            $table->datetime('date_sortie')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
