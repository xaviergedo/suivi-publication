<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomaineCompetenceUserPivotTable extends Migration
{
    public function up()
    {
        Schema::create('domaine_competence_user', function (Blueprint $table) {
            $table->unsignedBigInteger('user_id');
            $table->foreign('user_id', 'user_id_fk_53647850')->references('id')->on('users')->onDelete('cascade');
            $table->unsignedBigInteger('domaine_competence_id');
            $table->foreign('domaine_competence_id', 'domaine_id_fk_53647850')->references('id')->on('domaine_competences')->onDelete('cascade');
        });
    }
}
