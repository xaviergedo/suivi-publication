<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCourriersTable extends Migration
{
    public function up()
    {
        Schema::create('courriers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('numero')->nullable();
            $table->string('numero_registre')->nullable();
            $table->string('objet');
            $table->string('references');
            $table->string('type')->nullable();
            $table->datetime('date_enregistree')->nullable();
            $table->string('acteur')->nullable();
            $table->longText('commentaire')->nullable();
            $table->string('archiver')->nullable();
            $table->integer('last_etat')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }
}
