<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAffectationCourrierPivotTable extends Migration
{
    public function up()
    {
        Schema::create('affectation_courrier', function (Blueprint $table) {
            $table->unsignedBigInteger('affectation_id');
            $table->foreign('affectation_id', 'affectation_id_fk_5364785')->references('id')->on('affectations')->onDelete('cascade');
            $table->unsignedBigInteger('courrier_id');
            $table->foreign('courrier_id', 'courrier_id_fk_5364785')->references('id')->on('courriers')->onDelete('cascade');
        });
    }
}
