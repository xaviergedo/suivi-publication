<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    public function run()
    {
        $permissions = [
            [
                'id'    => 1,
                'title' => 'user_management_access',
            ],
            [
                'id'    => 2,
                'title' => 'permission_create',
            ],
            [
                'id'    => 3,
                'title' => 'permission_edit',
            ],
            [
                'id'    => 4,
                'title' => 'permission_show',
            ],
            [
                'id'    => 5,
                'title' => 'permission_delete',
            ],
            [
                'id'    => 6,
                'title' => 'permission_access',
            ],
            [
                'id'    => 7,
                'title' => 'role_create',
            ],
            [
                'id'    => 8,
                'title' => 'role_edit',
            ],
            [
                'id'    => 9,
                'title' => 'role_show',
            ],
            [
                'id'    => 10,
                'title' => 'role_delete',
            ],
            [
                'id'    => 11,
                'title' => 'role_access',
            ],
            [
                'id'    => 12,
                'title' => 'user_create',
            ],
            [
                'id'    => 13,
                'title' => 'user_edit',
            ],
            [
                'id'    => 14,
                'title' => 'user_show',
            ],
            [
                'id'    => 15,
                'title' => 'user_delete',
            ],
            [
                'id'    => 16,
                'title' => 'user_access',
            ],
            [
                'id'    => 17,
                'title' => 'configuration_access',
            ],
            [
                'id'    => 18,
                'title' => 'mouvement_access',
            ],
            [
                'id'    => 19,
                'title' => 'direction_create',
            ],
            [
                'id'    => 20,
                'title' => 'direction_edit',
            ],
            [
                'id'    => 21,
                'title' => 'direction_show',
            ],
            [
                'id'    => 22,
                'title' => 'direction_delete',
            ],
            [
                'id'    => 23,
                'title' => 'direction_access',
            ],
            [
                'id'    => 24,
                'title' => 'mention_create',
            ],
            [
                'id'    => 25,
                'title' => 'mention_edit',
            ],
            [
                'id'    => 26,
                'title' => 'mention_show',
            ],
            [
                'id'    => 27,
                'title' => 'mention_delete',
            ],
            [
                'id'    => 28,
                'title' => 'mention_access',
            ],
            [
                'id'    => 29,
                'title' => 'registre_show',
            ],
            [
                'id'    => 30,
                'title' => 'registre_access',
            ],
            [
                'id'    => 31,
                'title' => 'courrier_create',
            ],
            [
                'id'    => 32,
                'title' => 'courrier_edit',
            ],
            [
                'id'    => 33,
                'title' => 'courrier_show',
            ],
            [
                'id'    => 34,
                'title' => 'courrier_delete',
            ],
            [
                'id'    => 35,
                'title' => 'courrier_access',
            ],
            [
                'id'    => 36,
                'title' => 'affectation_create',
            ],
            [
                'id'    => 37,
                'title' => 'affectation_edit',
            ],
            [
                'id'    => 38,
                'title' => 'affectation_show',
            ],
            [
                'id'    => 39,
                'title' => 'affectation_delete',
            ],
            [
                'id'    => 40,
                'title' => 'affectation_access',
            ],
            [
                'id'    => 41,
                'title' => 'traitement_create',
            ],
            [
                'id'    => 42,
                'title' => 'traitement_edit',
            ],
            [
                'id'    => 43,
                'title' => 'traitement_show',
            ],
            [
                'id'    => 44,
                'title' => 'traitement_delete',
            ],
            [
                'id'    => 45,
                'title' => 'traitement_access',
            ],
            [
                'id'    => 46,
                'title' => 'audit_log_show',
            ],
            [
                'id'    => 47,
                'title' => 'audit_log_access',
            ],
            [
                'id'    => 48,
                'title' => 'faq_management_access',
            ],
            [
                'id'    => 49,
                'title' => 'faq_category_create',
            ],
            [
                'id'    => 50,
                'title' => 'faq_category_edit',
            ],
            [
                'id'    => 51,
                'title' => 'faq_category_show',
            ],
            [
                'id'    => 52,
                'title' => 'faq_category_delete',
            ],
            [
                'id'    => 53,
                'title' => 'faq_category_access',
            ],
            [
                'id'    => 54,
                'title' => 'faq_question_create',
            ],
            [
                'id'    => 55,
                'title' => 'faq_question_edit',
            ],
            [
                'id'    => 56,
                'title' => 'faq_question_show',
            ],
            [
                'id'    => 57,
                'title' => 'faq_question_delete',
            ],
            [
                'id'    => 58,
                'title' => 'faq_question_access',
            ],
            [
                'id'    => 59,
                'title' => 'etat_create',
            ],
            [
                'id'    => 60,
                'title' => 'etat_edit',
            ],
            [
                'id'    => 61,
                'title' => 'etat_show',
            ],
            [
                'id'    => 62,
                'title' => 'etat_delete',
            ],
            [
                'id'    => 63,
                'title' => 'etat_access',
            ],
            [
                'id'    => 64,
                'title' => 'profile_password_edit',
            ],
        ];

        Permission::insert($permissions);
    }
}
