<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    public function run()
    {
        $roles = [
            [
                'id'    => 1,
                'title' => 'Administrateur',
            ],
            [
                'id'    => 2,
                'title' => 'User',
            ],
            [
                'id'    => 3,
                'title' => 'Relecteur',
            ],
            [
                'id'    => 4,
                'title' => 'Auteur',
            ],
            [
                'id'    => 5,
                'title' => 'Superviseur',
            ],
        ];

        Role::insert($roles);
    }
}
