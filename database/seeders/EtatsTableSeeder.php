<?php

namespace Database\Seeders;

use App\Models\Etat;
use Illuminate\Database\Seeder;

class EtatsTableSeeder extends Seeder
{
    public function run()
    {
        $etats = [
            [
                'id'    => 1,
                'libelle' => 'Pour information',
            ],
            [
                'id'    => 2,
                'libelle' => 'En cours',
            ],
            [
                'id'    => 3,
                'libelle' => 'Traité',
            ],
            [
                'id'    => 4,
                'libelle' => 'Problème',
            ],
            [
                'id'    => 5,
                'libelle' => 'Abandonné',
            ],
            [
                'id'    => 6,
                'libelle' => 'Non traité',
            ],
        ];

        Etat::insert($etats);
    }
}
