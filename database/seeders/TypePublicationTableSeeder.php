<?php

namespace Database\Seeders;

use App\Models\TypePublication;
use Illuminate\Database\Seeder;

class TypePublicationTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $typePublications = [
            [
                'id'    => 1,
                'libelle' => 'Article',
            ],
            [
                'id'    => 2,
                'libelle' => 'Post',
            ],
            [
                'id'    => 3,
                'libelle' => 'Livre',
            ],
        ];
        TypePublication::insert($typePublications);
    }
}
