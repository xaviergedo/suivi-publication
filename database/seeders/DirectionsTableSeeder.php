<?php

namespace Database\Seeders;

use App\Models\Direction;
use Illuminate\Database\Seeder;

class DirectionsTableSeeder extends Seeder
{
    public function run()
    {
        $directions = [
            [
                'id'    => 1,
                'sigle' => 'DI',
                'description' => 'Direction de l\'Informatique',
                'type' => 0
            ],
            [
                'id'    => 2,
                'sigle' => 'DO',
                'description' => 'Direction des Opérations',
                'type' => 0
            ],
            [
                'id'    => 3,
                'sigle' => 'DA',
                'description' => 'Direction de l\'Administration',
                'type' => 0
            ],
            [
                'id'    => 4,
                'sigle' => 'DAJC',
                'description' => 'Direction des Affaires Juridiques et du Contentieux',
                'type' => 0
            ],
            [
                'id'    => 5,
                'sigle' => 'DAIC',
                'description' => 'Direction de l\'Audit Interne et de la Conformité',
                'type' => 0
            ],
            [
                'id'    => 6,
                'sigle' => 'DSP',
                'description' => 'Direction de Suivi des Projets',
                'type' => 0
            ],
            [
                'id'    => 7,
                'sigle' => 'DG',
                'description' => 'Direction Générale',
                'type' => 0
            ],
            [
                'id'    => 8,
                'sigle' => 'DM',
                'description' => 'Direction des Mobilisations',
                'type' => 0
            ],
            [
                'id'    => 9,
                'sigle' => 'DCom',
                'description' => 'Direction de la Communication',
                'type' => 0
            ],
            [
                'id'    => 10,
                'sigle' => 'SG',
                'description' => 'Secrétariat Général',
                'type' => 0
            ],
            [
                'id'    => 11,
                'sigle' => 'UTFED',
                'description' => 'UNITE D\'APPUI A L\'ORDONATEUR NATIONAL DU FONDS EUROPEEN DE DEVELOPPEMENT',
                'type' => 1
            ],
        ];

        Direction::insert($directions);
    }
}
