<?php

namespace Database\Seeders;

use App\Models\DomaineCompetence;
use Illuminate\Database\Seeder;

class DomaineCompetenceTableSeeder extends Seeder
{

    public function run()
    {
        $domaines = [
        [
            'id'    => 1,
            'libelle' => 'Expression orale/écrite',
        ],
        [
            'id'    => 2,
            'libelle' => 'Gestion de projet',
        ],
        [
            'id'    => 3,
            'libelle' => 'Programmation Python',
        ],
        [
            'id'    => 4,
            'libelle' => 'Marketing réseaux sociaux',
        ],
        [
            'id'    => 5,
            'libelle' => 'Gestion des conflits',
        ],
    ];
        DomaineCompetence::insert($domaines);
    }
}
