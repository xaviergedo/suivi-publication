<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call([
            PermissionsTableSeeder::class,
            RolesTableSeeder::class,
            EtatsTableSeeder::class,
            MentionsTableSeeder::class,
            PermissionRoleTableSeeder::class,
            DirectionsTableSeeder::class,
            UsersTableSeeder::class,
            RoleUserTableSeeder::class,
            DomaineCompetenceTableSeeder::class,
            TypePublicationTableSeeder::class,
        ]);
    }
}
