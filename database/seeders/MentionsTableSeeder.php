<?php

namespace Database\Seeders;

use App\Models\Mention;
use Illuminate\Database\Seeder;

class MentionsTableSeeder extends Seeder
{
    public function run()
    {
        $mentions = [
            [
                'id'    => 1,
                'libelle' => 'Bon à sortie',
            ],
            [
                'id'    => 2,
                'libelle' => 'A retraité',
            ],
        ];

        Mention::insert($mentions);
    }
}
